<div dir="rtl">بنام خدا</div>

- [Hardening Your System](#hardening-your-system)
- [Hardening User Passwords](#hardening-user-passwords)
- [Monitoring](#monitoring)
  - [Major logs](#major-logs)
  - [Monitoring Tools](#monitoring-tools)
- [Apps](#apps)
  - [Default Apps](#default-apps) 
  - [None Default Apps](#noen-default-apps)
- [Certified](#certified)
- [Penetration Checking](#penetration)
  - [Are you Under Attack](#are-you-under-attack)

[top](#)
# Hardening Your System
## 1- Physically
1. Configure the **BIOS** to _disable_ booting from CD/DVD, External Devices, Floppy Drive in BIOS. 
2. Enable BIOS **Password** 
3. **Protect** GRUB with password to _restrict physical access of your system_.

[top](#)
### 2- Partitioning
**Make Each Main directive _different Partition_**

### 3- Stop And Remove Unnecessary Services
- first list them:
```bash
  /sbin/chkconfig --list |grep '3:on'
```
- next disable and remove if useless:
```bash
  systemctl stop ServiceName.service
  systemctl disable ServiceName.service
  yum remove ServiceAppName -y
```
* Services like: _Telnet Server_,_RSH Server_,_NIS Server_,_Tftp Server_,_Talk Server_ and so on.
### 4- Check All Opening Ports
it is important to check all opened **ports** and close which ones you don't need.
```bash
  ss -tupln
### 5- [Make your ssh Secure](#ssh)
```
### 6- **Keep _SELinux_ and _IPTbles_ on forever**
[top](#)
### 7- becare on Crontab Jobs
it is safe deny all users in _/etc/cron.deny_. if Some users should have access, could write their name in _/etc/cron.allow_
```bash
  echo ALL > /etc/cron.deny
```
### 8- Change Some Files Permissions
```bash
  chown root:root /etc/{fstab,grub.conf,shadow,passwd,gpasswd,cron*,sysconfig/iptables,ssh/sshd_config}
  chmod 600 /etc/{grub.conf,shadow,cron*,sysconfig/iptables,ssh/sshd_config}
  chmod 644 /etc/{passwd,gpasswd,cron*}
  chown root:root -R /boot
  chmod 666 /boot
  chmod 766 -R /boot/*
```

### 9- Disable USB
```bash
  echo 'install usb-storage /bin/true' > /etc/modeprobe.d/no-usb
```
### 10- Suggested to remove X11
```bash
  yum grougremove "X Window System"
```
### 11- [Users Password Policy](#hardening-user-passwords)
### 12- Disable Ctrl+Alt+Delete
- Simple way is:
```bash
  systemctl mask ctrl-alt-del.target
```
- [another way](https://serverfault.com/a/866090/209634)
```bash
  # ctrl-alt-del.target
  [Unit]
  DefaultDependencies=no  # Do not affect the system through dependencies
  Requires=ctrl-alt-del.service
  StopWhenUneeded=yes
  
  # ctrl-alt-del.service
  [Service]
  DefaultDependencies=no  # Do not affect the system through dependencies
  Type=oneshot
  ExecStart=/usr/bin/logger -p security.info "Control-Alt-Delete pressed"
```
### 13- Make /boot as _readonly_
- Main way: edit _/etc/fstab_ and add below:
```bash
    LABEL=/boot     /boot     ext2     defaults,ro     1 2
```

[top](#)
# Hardening User Passwords
### 1- Force Users to use Strong Password
- the old way is adding below line in _/etc/pam.d/system.auth_:
```bash
  /lib/security/$ISA/pam_cracklib.so retry=3 minlen=8 lcredit=-1 ucredit=-2 dcredit=-2 ocredit=-1
```
- but in new versions, you should modify _/etc/security/pwquality.conf_ file.
### 2- Set Users Password Exiraty
1- Set it for all: chnage below items in _/etc/login.defs_ file:
```bash
  PASS_MAX_DAYS 40
  PASS_MIN_DAYS 8
  PASS_WARN_AGE 5
```
2- Set for each User:
```bash
  chage -M 40 -m 8 -W 5 UserName
```
* to check an existing User information: `chage -l UserName`
[top](#)  
### 3- Restrict Users to use previous Passwords
- modifying _/etc/pam.d/system-auth_ file:

  1- add below in **auth** part:
  ```bash
    auth sufficient pam_unix_info.so likeauth nullok
  ```
  2- and in _password_ section:
  ```bash
    password sufficient pam_unix_info.so nullok use_authok md5 shadow remember=3
  ```
### 4- Lock User if wronge password 3 times
- in _/etc/pam.d/system-auth_:
```bash
  auth required pam_faillock.so preauth audit silent deny=5 unlock_time=604800
  auth [success=1 default=bad] pam_unix.so
  auth [default=die] pam_faillock.so authfail audit deny=5 unlock_time=604800
  auth sufficient pam_faillock.so authsucc audit deny=5 unlock_time=604800
  auth required pam_deny.so
```
- and in _/etc/pam.d/password-auth_:
```bash
  auth required pam_faillock.so preauth audit silent deny=5 unlock_time=604800
  auth [success=1 default=bad] pam_unix.so
  auth [default=die] pam_faillock.so authfail audit deny=5 unlock_time=604800
  auth sufficient pam_faillock.so authsucc audit deny=5 unlock_time=604800
  auth required pam_deny.so
```
* to unlock user : `/usr/sbin/faillock --user UserName --reset`

### 5- Disable System Users
- use this bash script:
```bash
  #!/bin/bash
  for user in `awk -F: '($3 < 500) {print $1 }' /etc/passwd`; do
    if [ $user != "root" ]; then
      /usr/sbin/usermod -L $user
      if [ $user != "sync" ] && [ $user != "shutdown" ] && [ $user != "halt" ]; then 
        /usr/sbin/usermod -s /sbin/nologin $user
      fi
    fi
  done
```
[top](#)
# Monitoring
- [Major logs](#major-logs)
- [Monitoring Tools](#monitoring-tools)
  - [Who are logins](#who-are-logins)
  - [PsAcct](#psacct)
### Major logs
- /var/log/message: Where whole system logs or current activity logs are available.
- /var/log/auth.log: Authentication logs.
- /var/log/kern.log: Kernel logs.
- /var/log/cron.log: Crond logs (cron job).
- /var/log/maillog: Mail server logs.
- /var/log/boot.log: System boot log.
- /var/log/mysqld.log: MySQL database server log file.
- /var/log/secure: Authentication log.
- /var/log/utmp.or /var/log/wtmp: Login records file.
- /var/log/yum.log: Yum log files.
##### Journalctl
this is a new Log controling tool.
```bash
  journalctl <--------------- to show All
  journalctl -xf <----------- Online Log monitoring
  journalctl -u Name <------- Monitor specific Package,Service,...
```

[top](#)
### Monitoring Tools
- [Who are logins](#who-are-logins)
- [PsAcct](#psacct)
  - [AC Command](#ac-command)
  - [SA Command](#sa-command)
  - [LastComm Command](#lastcomm-command)
#### Who are logins
- who: who connected.
- w: who are connected and what is last command
- last: last commands for each users
- lastlog: 
- cat /var/log/messages
##### lsof:
  - list of Established Connection : 
  ```vim
   lsof -i TCP:80 | grep ESTABLISHED` or `watch "lsof -i TCP:80"
  ```
[top](#)
### PsAcct
##### AC Command
this is for print statistics about users connect time.
```bash
  ac [UserName] <------ Totally Statistics
  ac -d <-------------- Show per Days
  ac -p <-------------- Show per Users
```

##### SA Command
Use to show summary of user command information
```bash
  sa <----------------- Totally of Commands
  sa -u <-------------- Show Which user which commands
  sa -m <-------------- How many process taken with which user
  sa -c <-------------- Show Users percentage used
```
[top](#)
##### LastComm Command
Used to show previously commands used with which user
```bash
  lastcomm [UserName|Command]
```
  
[top](#top)

[top](#)
#### PsAcct
 
[top](#)

# Apps
- [Default Apps](#default-apps) 
  - [Audit](#audit)
  - [SeLinux](#selinux)
  - [SSH](#ssh)
- [None Default Apps](#noen-default-apps)
  - [Fail2Ban](#fail2ban)

## Default Apps
### Audit
- Configuring Audit:
 - num_logs=No.
 - max_log_file = No. --> in MB
 - max_log_file_action = ROTATE
 - log contain: for `auditctl -w /etc/ssh/sshd_config -p rwxa -k sshconfigchange` we'd find below record in log file

    type=SYSCALL,msg=audit(1434371271.277:135496):,arch=c000003e(cpu info),
    syscall=2(use `ausyscall 2` to unserstand),success=yes,ppid=6265,pid=6266,auid=1000,
    uid=0,comm="cat",exe="/usr/bin/cat",key="sshconfigchange"
 -
 ```vim
    ausearch -m LOGIN --start today -i
    ausearch -a 27020
    ausearch -f /etc/ssh/sshd_config -i
    aureport -x --summary
    aureport --failed
 ```
- remove Unnecessary packages: `yum erase inetd xinetd ypserv tftp-server telnet-server rsh-serve`
- Password Policy
 - User Accounts and Strong Password Policy:
  - Password Aging with `chage` command. Like 
  ```vim
    chage -M 60 -m 7 -W 7 userName
  ```
  - use >/etc/login.defs
  - in ;/etc/shadow; file:
  ```vim
    {userName}:{password}:{lastpasswdchanged}:{Minimum_days}:{Maximum_days}:{Warn}:{Inactive}:{Expire}:
  ```
 - Restricting Use of Previous Passwords:
 - Locking User Accounts After Login Failures:
 - How Do I Verify No Accounts Have Empty Passwords?: 
 ```vim
   awk -F: '($2 == "") {print}' /etc/shadow` and Lock them `passwd -l UserName
 ```
 - Make Sure No Non-Root Accounts Have UID Set To 0 : 
 ```vim
   awk -F: '($3 == "0") {print}' /etc/passwd
 ```
 [top](#top)
### SeLinux
  - Note:
    + SELinux Access Control : 1-Type Enforcement (TE) 2-Role-Based Access Control (RBAC) 3-Multi-Level Security (MLS)
    + ls -Z --> user:role:type:mls
    + fundamental reason may seLinux prevent access to file or service or app:
      1. a mislabled file
      2. a process running under wrong selinux security context
      3. a bug in policy.an app needs acesses to a file that wasn't anticipaited
      4. an intrusion attempt
  - `sestatus`
  - `sealert`:
  ```vim
    sealert -a /var/log/audit/audit.log > /path/to/mylogfile.txt
  ```
  - `chcon`:
    ```vim
      chcon -v --type=httpd_sys_content_t /html
      chcon -Rv --type=httpd_sys_content_t /html <- recursively
    ```
  - `restorecon` :
    ```vim
      restorecon -Rv /var/www/html
      restorecon -Rv -n /var/www/html --> only show the default
      touch /.autorelabel <- to automaticaly relabled FileSystem after reboot
    ```
  - `semanage` :
    ```vim
      semanage port -a -t http_port_t -p tcp 81 <- open port
      semanage port -l <- Check Port is reserved
      semanage fcontext -a -t httpd_sys_content_t "/html(/.\*)?" <- add new labled for futurs files will be coming
    ```
  - Create SeModule:
    - Way 1:
      ```vim
        grep smtpd_t /var/log/audit/audit.log | audit2allow -m postgreylocal > postgreylocal.te && cat postgreylocal.te
        grep smtpd_t /var/log/audit/audit.log | audit2allow -M postgreylocal 
        semodule -i postgreylocal.pp
      ```
    - Way 2:
      ```vim
        grep postdrop /var/log/audit/audit.log | audit2allow -M postfixlocal
        cat postfixlocal.te
        dontaudit postfix_postdrop_t httpd_log_t:file getattr; <- in .te file
        checkmodule -M -m -o postfixlocal.mod postfixlocal.te
        semodule_package -o postfixlocal.pp -m postfixlocal.mod
        semodule -i postfixlocal.pp
      ```
  - getsebool -a
  - seinfo --portcon=80
  - setsebool -P BooleanParameter 1
  
[top](#top)

### SSH
#### sshd_config:
 - __Port__ _must Change it_
 - __PermitRootLogin__ _yes|no_
 - __AuthenticationMethods__ "_publickey,password_" "_publickey,keyboard-interactive_"
 - __RSAAuthentication__ _yes_
 - __PubkeyAuthentication__ _yes_
 - Specified Special Hosts: to make type of connect , Which Users Allowed , ... .
 ```vim
   Match Address IPAddress
     AlloewUsers UserName1 UserName2
     AuthenticationMethodes "publickey"|"authentication"
   Match User Name1,Name2 Address IP
     AuthenticationMethodes 
 ```
[top](#top)
#### Key:
 1. `ssh-keygen -t RSA`
 2. `ssh-copy-id -i PathToSSHKey User@Server`

[top](#top)
#### ssh_config
 - Special Host:
 ```vala
  Host Name
    HostName IP
    Port
  Host IP
    Port
 ```
- config: to specify which Host should use wich Key to connect
```vala
  Host HostName
    IdentityFile ~/.ssh/Host_Private_Key_Name
```
* if you'd like to prevent ssh connection time out issue, you have two way:
  - Client side:<br>
    1- permanent: add below to _.ssh/config_:
    ```vala
      ServerAliveInterval 120
    ```
    2- runtime:
    ```vala
      ssh -o ""ServerAliveInterval 60" user@...
    ```
  - Server Side: add below into _/etc/sshd?sshd_cinfig_:
  ```vala
    ClientAliveInterval 120
    ClientAliveCountMax 720
  ```

[top](#top)
#### Save Key Passphrase:
  1. First connect to your remote server.
  2. in another terminal run `ssh-add` command
  - may you need check `ssh-agent` app
- Scape SSH without disconnecting:
  - type <kbd>\~</kbd> and press <kbd>Ctrl</kbd><kbd>Z</kbd>, it take you back to your local.
  - for return to _ssh_ check the _jobs_ in your terminal :smile:

[top](#top)
#### [Restrict access by time](https://www.techrepublic.com/article/using-pam-to-restrict-access-based-on-time/)
1. first we should define or policy in _time.conf_ pam file:
```vim
  sshd;*;UserName;AlTime1-Time2
```
  - in explanation:
    - first column indicates _the Service Name_ here is __sshd__
    - second column indicates _TTY Number_ here is __all ttys__
    - third indicates UserName
    - fourd indicates _StartTime2EndTime_, we should use ___Al___ at the first
2. we should tell to sshd service to use thid __PAM__ module, then add below to _/etc/pam.d/__sshd___ file:
```vim
  account required pam_time.so
```

[top](#top)
## None Defualt Apps 
### Fail2Ban:
  - Default:bantime,maxretry,enabled,banaction,action
  - [ssh _d_]:filter,port,maxretry 
  - [...]: filter=[...]
- Check if you are under DDoS attack:
```vim
  netstat -anp |grep 'tcp\|udp' | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort –n
```
- Conntrack : track connections
 - is module loaded : `modeprobe nf_conntrack_ipv4`
 - installation `yum install conntrack-tools libnetfilter_conntrack`
 - report 
 ```vim
   conntrack -L -C  -E -e NEW  -p tcp --state ESTABLISHED --dport 22
 ```
 - there is contrack app as daemon with _conntrackd_ name that you could find it's info.
 - go out from jail : 
  1. `iptables -L -n`
  2. `fail2ban-client set YOURJAILNAMEHERE unbanip IPADDRESSHERE`
- Unban an IP:
 - find which IP:`iptables -L -n`
 - get it out : `fail2ban-client set YOURJAILNAMEHERE unbanip IPADDRESSHERE`

[top](#top)
# Certified
- generate Cert
  ```vim
    openssl genrsa -des3 -out server.key 1024
    openssl req -new -key server.key -out server.csr
    cp server.key server.key.org && openssl rsa -in server.key.org -out server.key <- you may need remove pass phrase
    openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
  ```
> _also we could generate them in one line_

```vim
  openssl req -x509 -nodes -sha256 -days 365 -newkey rsa:4096 -keyout Key.key -out Cert.crt
```
> [to check the security of certificate](https://shaaaaaaaaaaaaa.com/)

[top](#top)
# Are you Under Attack
- DDoS Attack : 
```vim
  netstat -anp |grep 'tcp\|udp' | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort
```
- DDoS on Port : 
```vim
  netstat -n | grep :80 |wc -l
```
- on Packet Type : 
```vim
  netstat -n | grep :80 | grep SYN |wc -l
```
- Under ICMP : 
```vim
  while :; do netstat -s| grep -i icmp | egrep 'received|sent' ; sleep 1; done
```
- Under a SYN flood : 
```vim
  netstat -nap | grep SYN
```

[top](#top)

