#!/bin/bash
Dir=`dirname "$0"`
for File in $(cat $Dir/Apps)
do
  IFS=',' read -ra NFile <<< "$File"
#  if [[ "${NFile[0]}" == '61'* ]] || [[ "${NFile[0]}" == 'cloud' ]] 
  if [[ "${NFile[0]}" == $1 ]]
  then 
    if [[ $2 == 'delete' ]] 
    then
      rm -f "${NFile[3]}" 
    fi
  fi
done
for UserPass in $(cat $Dir/UserNames)
do
  IFS=',' read -ra NUserPass <<< "$UserPass"
#echo "${NUserPass[0]}" Main Pass id  "${NUserPass[1]}"
  for App in $(cat $Dir/Apps)
  do
    IFS=',' read -ra NApp <<< "$App"
    if [[ "${NApp[0]}" == $1 ]]
    then
        NPass=$(lesspass "${NApp[2]}" "${NUserPass[0]}" "${NUserPass[1]}" -L=12)
  #echo lesspass "${NApp[2]}" "${NUserPass[0]}" "${NUserPass[1]}" -L=12
      OpenSSl=`openssl passwd -apr1 "'${NPass}'"`
  #echo openssl passwd -apr1 "'${NPass}'"
      echo "${NUserPass[0]}":"${OpenSSl}" >> "${NApp[3]}"
  #echo "${NUserPass[0]}":"${OpenSSl}" --\> "${NApp[3]}"
  #echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
      echo "${NUserPass[0]}" --\> "'${NPass}'" --\> "${NApp[4]}"  ,the Url is -->"${NApp[2]}" >> "${NUserPass[0]}"
  #   if [ "${NApp[1]}" == "RSJ" ]
  #   then
  #echo $NPass  "------------------------->"  "${NUserPass[0]}" "----/>" "${NApp[2]}"
  #   fi
    fi
  done
 echo  "${NUserPass[1]}" | gpg --batch --no-tty --yes --passphrase-fd 0 --symmetric -o "${NUserPass[0]}".gpg "${NUserPass[0]}"
 rm -f "${NUserPass[0]}"
done
