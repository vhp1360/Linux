<div dir="rtl">بنام خدا</div>

- [Bash Environments](#bash-environments)
  - [Special Meta-Data](#special-meta-dat)
  - [Variables](#variables)
  - [Special Characters](#special-characters)
  - [I/O Redirection](#i/o-redirection)
  - [Bash Options](#bash-options)
  - [Keyword Combination](#keyword-combination)
- [GETOPTS Powerfull Variable in Bash](#getopts-powerfull-variable-in-bash)  
- [Bash Processes](#bash-processes)
  - [Bash Subshells](#bash-subshells)
  - [ProcessID Varibales](#processid-variables)
  - [Process Substitution](#process-substitution)
- [Bash Version4](#bash-version4)
- [Bash Commands](BashCommands.md)
- [Bash Command line tools](#bash-command-line-tools)
  - [Command Line Web Browsing](#command-line-web-browsing)
- [Jobs](#jobs)
- [Users](#users)
  - [Change username](#change-username)
  - [useradd](#useradd)
  - [usermod](#usermod)
- [IPV6](#ipv6)
  - [disabling IPv6](#disablingipv6)
- [VPN Routes](#vpn-routes)
- [Scripting](#scripting)
  - [Logging](#logging)
  - [Variable](#variable)
  - [Input Prameters](#input-parameters)
  - [Loop](#loop)
  - [Conditional](#conditional)
- [Text Processing](#text-processing)
  - [REGEX](#regex)
    - [Regex Table](#regex-table)
  - [AWK](#awk)
  - [CUT](#cut)
  - [SED](#sed)
  - [Sort](#sort)
  - [Uniq](#uniq)
  - [Head and Tail](#head-and-tail)
  - [Grep](#grep)
  - [Wc](#wc)
  - [Tr](#tr)
  - [Paste](#paste)
  - [Join](#join)
  - [Comm](#comm)
  - [Diff](#diff)
  - [Test Constructs](#test-constructs)
  
- [Unicode](#unicode)
  - [Convert to Ascii](#convert-to-ascii)
  - [Convert to each Other](#convert-to-each-other)
- [ulimit](#ulimit)


[top](#top)
# Bash Environments
- [Special Meta-Data](#special-meta-dat)
- [Variables](#variables)
- [Special Characters](#special-characters)
- [Enviroments](#enviroments)
 

[top](#)

# Special Meta-Data
there are some settings those affect whole of bash:
1. `set verbose` or `set -v`: echo each line of script before execute
2. `set echo` or `set -x`   : echo each line of script after substitution
3. 

use `unset` instead of `set` to nullify them or `set +[x|v]`
[top](#)

# Variable
- [Internal Variables](#internal-variables)
  - [Globbing](#globbing)
- [Variable Substitution](#variable-substitution)
  - [Variable Substitution inside another Variable](#variable-substitution-inside-another-variable)
- [Bash Variables Types](#bash-variables-types)
  - [Variables Definition](#variables-definition)
  - [Internal Variables](#internal-variables)


## Internal Variables
- [Bash Env](#bash-env)
- [Directories](#directories)
- [Editor](#editor)
- [User And Group](#user-and-group)
- [OS Virables](#os-variables)
- [Data Working](#data-working)
- 

[top](#internal-variable)

### Bash Env
- _$BASH_
- _$BASH_ENV_
- _$BASH_SUBSHELL_
- _$BASHPID_ :left_right_arrow: **$$**
- _$BASH_VERSION_ 
- _$BASH_VERSINFO[n]_ 
```bash
  for i in 0..5;do
    echo -e "BASH_VERSION_INFO[$i]=" ${BASH_VERSINFO[$i]} "\n"
  done
```
- _$SHELLOTPS_: list all shell options
- _$SHLVL_: what is current Shell Level
- _$TMOUT_: how many seconds to _timeout_ shell if set to _none zero_ value.
- 


[top](#internal-variable)
### Directories
- _$PATH_
- _$CDPATH_: like [PATH](#path) just for _`cd`_ command.
```vim
  CDPATH=/etc/nginx:/var:...
  cd nginx
```
- _$DIRSTACK_: could `pushd directories` to this and do sometings,get back from each with `popd` that use _DIRSTACK_ variables.
```bash
  pushd ~/Documents  <-- put you in ~/Documents and add this to DIRSTACK
  ...
  pushd ~/Downloads
  ...
  pushd ...
  ...
  popd  <--- get back in latest path
```
- _$HOME_
- _$OLDPWD_
- 


[top](#internal-variable)
### Default Editor
- _$EDITOR_
### USER And Group
- _$UID_
- _$EUID_
- _$GROUPS_: list groups that current user belong to.
[top](#internal-variable)
### OS Variables
- _$HOSTNAME_
- _$HOSTTYPE_
- _$MACHTYPE_: return _machine System type_
- _$OSTYPE_
- _$PIPESTATUS_: an Array that keeps _Exit_ status of _Piped_ commands
- _$PID_
- _$PPID_
- _$PROMPT_COMMAND_
- _$PS1_: this is a main prompt 
- _$PS2_: secondary prompt seen when additionl command expected. shown as "**>**" sign
- _$PS3_: tertiary,displayed in **select Lopp**
- _$PS4_: quartenary,

[top](#internal-variable)
### Data Working
- _$IFS_: internal field seperator
- _$LINENO_: return _line number_ of current _shell script_
- _$REPLY_: the default value of **read** command when _vriable_ not _supplied_
- _$SECONDS_: how many seconds the script runs.
- _$0,$1,..._: **Positional Prameters**
- _$#,$*,$@_: Number of Command-line Arguments,All input **as one word** and should call with _"_,same as previous but **each variable as quoted String**
  * The $@ and $* parameters differ only when between double quotes. 

[top](#internal-variable)
## Variable Substitution
```bash
  var1=123
  echo var1 ------> var1
  echo '$var1'----> var1
  echo $var1 -----> 123
  var2=var1 ------> var2=var1
  var2=$var1 -----> var2=123
  "var2= var1" ---> wrong: bash execute var1 with var2= Null environment
  "var2 =var1" ---> wrong: bash execute var2 with "=var1" input parameter
```  
[top](#variable)
### Variable Substitution inside another Variable
there a variable in file fi.source:
```
  v1='Hello $Name'
```
in the bash file:
```bash
  source fi.source
  Name="a Name"
  msg=$(eval echo $(echo $v1))
  echo msg
```
[top](#variable)

# Bash Variables Types
## Variables Definition
- **let**: command carries out arithmetic operations on variables.
- **expr**: All-purpose expression evaluator: Concatenates and evaluates the arguments according to the operation given.
```bash
  a=`expr 5 + 3`
  echo "a= $a" ----------------------> a= 8
  b=`expr $a \< 10`
  echo "b= $b" ----------------------> b= 1
  a= qweasdzxc
  echo "lentgh of a=\" $a \" is `expr length $a`"
  echo "first position of 'a' in $a is `expr index $a a`"
  echo "Substr of \" $a \" strat 3 til 6 chars `expr substr $a 3 6`"
  echo "Match Only from first qwe in \" $a \" `expr match "$a" 'qwe'`"
  echo "the first 5 chars of \"$a\" is `expr $a : ''\(.....\)'` and the last are `expr $a : ''.*\(.....\)`'" 
```
  * Using _**\(,\)** to make [Regex](Bash.md#regeg) Bash grouping.
  ```bash
    expr "$a" : '[[:space:]]*\(.*\)[[:space:]]*$'
  ```
- **eval**: Combines the arguments in an expression or list of expressions and evaluates them. Any variables within the expression are expanded. 
- **export**: make a variable visible to current and all child process.
- **declare/typeset**: Specify variable and may restrict it.

Option|Using
---|---
-r|readonly
-i|integer
-a|array
-f|function
-x|export
[top](#)

* The declare command can be helpful in identifying variables, environmental or otherwise. 
```bash
  declare | grep HOME ----------> HOME=/home/bozo
```
[top](#)
- **readonly**: Same as declare -r
- **Locale**: variable Only visibile inside the codeBolock. before _Calling Function_ all locale variables invisible for outside of block.
```bash
  t=$(exit 1)
  echo $? --------> return 1
  locale t
  t=$(exit 1)
  echo $? --------> return 1
  locale t=$(exit 1)
  echo $? --------> return 0 appearently, variable assignment assigning before locale defining.
```
- **Environmental**: effect shell and User Interface
- **unset**:
```bash
  let a=1
  let a+=5 ~ "a+=5"
  let a=4>5?1:2
  eval "a=3"
  set `uname -a` ---------> Sets the positional parameters to the output of the command `uname -a`
  set -- $variable -------> reassigning Positional Parameters
  shif 2 -----------------> shif Positional Parameter 2 times left, then $1 is $3 right now
  export column_number
  Var=$ENVIRON["column_number"]
  declare -r var=1
  declare -x var=2
```  
  
[top](#)


### mkdir and cd to that
use below code in your bash file
```vim
  mkcd() { mkdir -p "$@" && cd "$@" }
```
[top](#top)
## [Special Characters](https://www.tldp.org/LDP/abs/html/special-chars.html)
there is a table:

Character|Where|Meaning
---|---|---
\<RETURN\>|csh, sh|Execute command
#|csh, sh, ASCII files|Start a comment
\<SPACE\>|csh, sh|Argument separator
`|csh, sh|Command substitution
"|csh, sh|Weak Quotes
'|csh, sh|Strong Quotes
\\ |csh, sh|Single Character Quote
variable|sh, csh|Variable
variable|csh, sh|Same as variable
\||csh, sh|Pipe character
^|sh|Pipe Character
&|csh, sh|Run program in background
?|csh, sh|Match one character
*|csh, sh|Match any number of characters
;|csh, sh|Command separator
;;|sh|End of Case statement
~|csh|Home Directory
~user|csh|User's Home Directory
!|csh|History of Commands
-|Programs|Start of optional argument
$#|csh, sh|Number of arguments to script
$*|csh, sh|Arguments to script
$@|sh|Original arguments to script
$-|sh|Flags passed to shell
$?|sh|Status of previous command
$$|sh|Process identification number
$!|sh|PID of last background job
&&|sh|Short-circuit AND
\|\||sh|Short-circuit OR
.|csh, sh|Typ. filename extension
.|sh|Source a file and execute as command
:|sh|Nothing command
:|sh|Separates Values in environment variables
:|csh|Variable modifier
Character|Where|Meaning
[ ]|csh, sh|Match range of characters
[ ]|sh|Test
%job|csh|Identifies job Number
(cmd;cmd)|csh. sh|Runs cmd;cmd as a sub-shell
{ }|csh|In-line expansions
{cmd;cmd }|sh|Like (cmd;cmd ) without a subshell
\>ofile|csh, sh|Standard output
\>\>ofile|csh, sh|Append to standard output
\<ifile|csh, sh|Standard Input
\<\<word|csh, sh|Read until word, substitute variables
\<\<\word|csh, sh|Read until word, no substitution
\<\<-word|sh|Read until word, ignoring TABS
\>>!file|csh|Append to file, ignore error if not there
\>\!file|csh|Output to new file, ignore error if not there
\>\!file|csh|Output to new file, ignore error if not there
\>\!file|csh|Output to new file, ignore error if not there
\>\&file|csh|Send standard & error output to file
\<\&digit|sh|Switch Standard Input to file
\<\&-|sh|Close Standard Input
\>\&digit|sh|Switch Standard Output to file
\>\&-|sh|Close Standard Output
digit1\<&digit2|sh|Connect digit2 to digit1
digit\<\&-|sh|Close file digit
digit2\>\&digit1|sh|Connect digit2 to digit1
digit\>\&-|sh|Close file digit

[top](#top)

- [SemiColons](#semicolon)
- [SemiColon Amper Sign](#semicolon-amper-sign)
- [Dot](#dot)
- [Quoting](#quoting)
- [Commas](#commas)
- [Slashes](#slashes)
- [parentheses](#parentheses)
- [Exclamation Mark](#exclamation-mark)
- [Ad Sign](#ad-sign)
- [Pound Sign](#pound-sign)
- [Dollar Sign](#dollar-sign)
- [Percent Sign](#percent-sign)
- [Hat Sign](#hat-sign)
- [Amper Sign](#amper-sign)
- [Star Sign](#star-sign)
- [Character Cases](#character-cases)
- [Colon](#colon)
- [Inequality Signs](#inequality-signs)
- [Question Mark](#question-mark)
- [Pipe Sign](#pipe-sign)
- [Chain Commands](#chain-commands)
- [Dash Sign](#dash-sign)
- [Plus Sign](#plus-sign)
- [Equeal Sign](#equeal-sign)
- [Tilda Sign](#tilda-sign)
- [Math Signs](#math-signs)

[top](#)
### SemiColon
- Single or _\;_: is _command seperator_.
```bash
  for q in 1 2 3 4; do <--- becare on Space after semicolon
```
[top](#top)
- Double SemiColon or _\;\;_: for case Item termination.
```bash
  case $q in
    12|23) echo "1111111111111" ;;
  esac
```
  * in newer version **;&** or **;;&** is terminator.
### Dot
or _**.**_:
  1. for sourcing a file: `. FileName`
  2. for hidden file: `.FileName`
  3. current or parent directory: `ls . ; ls ..`
  4. as part of Regular Experssion.

[top](#top)
### Quoting
two types:
  1. Full Quoting that preserve all Special Characters as Character
  2. Partial Qouting that preserve Special Character as are
### Commas
1. links together a series of arithmetics:
```bash
  let "q=((w=3,8+4))" <--- will set q=12 and w=3
```
2. Concatinate Strings:
```bash
  for file in ~{,Documents}; do echo $file; done <--- will search in Home and Home/Documents
```
3. [Character cases](#character-cases)

[top](#)
### Slashes

[top](#)
### parentheses
1. One Pair Parentheses:

    1/1. Coomands Group
    ```bash
      ...
      (a=2; echo $a)
      ...
    ```
    * Note that command run in **parentheses** isnot _visibile_ for **Main Script** because _Command in parentheses_ run as **subprocess** and **SubProcess** couldnot _export varible back_  to **Parent Process**
  
    1/2. Array initialization:
    ```bash
      Array=(Elem1 Elem2 ...)
    ```
2. One Pair Braces:

    2/1. for expansion:
    ```bash
      echo {A,B,C}:{\ 1\ ," 2",' 3' ----> A: 1 A: 2 A: 3 B: 1 ...
    ```
    2/2. for extended:
    ```bash
      echo {a..z} ----> a b c d e ...
    ```
    2/3. Block of Code or inline function definition:
    ```bash
      a=123
      {a=456;}
      echo a ----> 456
    ```
    * Note: variables in Block Code would visible for remain of codes.
    2/4. delineate _do_ _done_ command for _for loop_ command:
    ```bash
      for((q=1;q<10;q++)){echo q;}
    ```
    2/5. Placeholder for text in **xargs** and **find** _command_:
    ```bash
      ls . | xargs -i -t cp ./{} $1
      find . -exec rm {} \;
    ```
3. One Pair Bracket:

    3/1. [Test Constructs](#test-constructs)
    3/2. Array Element
    3/3. range of characters in [Regex](#regex)
    3/4. Evaluate integer expression between _$[ ]_
    ```bash
      a=3;z=4;echo $[$a*$z]
    ```
    
4. Double Parentheses: Evaluate and Integer Expantion:
```bash
  echo $((10+12))
  ((q=a>z?10:12))
```
[top](#)
### Exclamation Mark
to reverse or negate.
[top](#top)
### Ad Sign
### Pound Sign
or _\#_ :
  1. is as Comment
  2. to showing String Length: `${#var}`
  3. Number of Positional Parameters: `${#*}` or `${#@}`
  4. Number of Array's Elements: `${array[*]}` or `${array[#*]}`
  5. remove _shortest_/_longest_ **match** _front_/_back_ **End** _$var_: `${var[#|%/##|%%]Pattern}`
  ```bash
    var=qqq.www.eee.rrr
    echo ${var#*.} ---> www.eee.rrr
    echo ${var%%.*} --> qqq
  ```
  6. as _BASE_#_NUMBER_:
  ```bash
    let "Octal=8#27" <--- like call: Octal=027
    let "Hex=16#ab7" <--- like call: Hex=0xab7
  ```
  7. as regular Char if prefiix with \" , \' , \\

[top](#top)
### Dollar Sign
#### alone Dollar Sing
1. Variable Substitution
```bash
  n="1 2  3   4     5"
  echo $n
  m=$n
```
2. as _part of_ [**Regular Experssion**](#regex)
#### With Other Sing
1. **${}**: [Parameter Substitution](https://www.tldp.org/LDP/abs/html/parameter-substitution.html):
```bash
  UserHost="${USER:None} on ${HOSTNAME:-Local}" <--- we could pass default value as two way
  echo ${NewVar=123} ~ ${NewVar:=123}  <------------------------- if parameter not set, set it to default vallue
  echo ${NewVar+123} ~ ${NewVar:+123}  <------------------------- if parameter Set, use default vallue else use Null value
  Var=${NewVar?"Error Message"} ~ ${NewVar:?"Error Message"}  <-- if parameter Set, use it otherwise raise error and print Error Message
  Var=${#Var} <-------------------------------------------------- Length of var variable
  Var=${#Arr[*]} <----------------------------------------------- Array Length
  Var=${#Arr[n]} <----------------------------------------------- Length of nth Array\'s element
  Var=${var#Pattern} <------------------------------------------- Remove shortest part of $Pattern from $var from front end of $var.
  Var=${var##Pattern} <------------------------------------------ Remove shortest part of $Pattern from $var from front end of $var.
  Var=${var%Pattern} <------------------------------------------- Remove shortest part of $Pattern from $var from back end of $var.
  Var=${var%%Pattern} <------------------------------------------ Remove shortest part of $Pattern from $var from back end of $var.
  Var=${var:pos} <----------------------------------------------- Variable var expanded, starting from offset pos.
  Var=${var:pos:len} <------------------------------------------- Expansion to a max of len characters of variable var, from offset pos.
  Var=${var/Pattern/Replacement} <------------------------------- First match of Pattern, within var replaced with Replacement.deleted If Replacement is omitted.
  Var=${var//Pattern/Replacement} <------------------------------ Global replacement. All matches of Pattern, within var replaced with Replacement.
  Var=${var/#Pattern/Replacement} <------------------------------ Prefix replacement of var.
  Var=${var/%Pattern/Replacement} <------------------------------ Suffix replacement of var.
  ${!varprefix*}, ${!varprefix@} <------------------------------- Matches names of all previously declared variables beginning with varprefix.

```
2. **$'...' or $"..."**: [Quote Strings](https://www.tldp.org/LDP/abs/html/escapingsection.html#STRQ)
3. **$#,$***: [Positional Parameters](https://www.tldp.org/LDP/abs/html/internalvariables.html#APPREF)
4. **$?**: return **Exit Status** of _Code_
5. **$$**: PID of current Script.
#### Double Dollar Sign
return _PID_ of Script

[top](#)
### Percent Sign
### Hat Sign
1. [Regex](#regex)
2. [Character Cases](#character-cases)

[top](#top)
### Amper Sign
for redirect [Job](#jobs) to [Background](#bg)
### Star Sign
1. as _Wild Card_ for [**Globbing**](#globbing)
2. as _part of_ [**Regular Experssion**](#regex)
3. as _arithmetic operation_

[top](#top)
### Colon
is _Null Command_ and like _true_ command *shell*.
1. as infinite for loop:
```bash
  while :; do echo True; done
```
2. as Nothing in _if_:
```bash
  if [Condition]; then :; else echo False; fi
```
3. a placeholder to execute command: all below are the same
```bash
  n=1
  : $((n=$n+1)) ~ n=$(($n+1)) ~ : $[n=$n+1] ~ n=$[$n+1] ~ ((n=n+1)) ~  let "n=n+1" ~ let "n=$n+1"
```
4. less usage as a _function Name_ and _start a comment_.

[top](#top)
### Inequality Signs
1. _: >_: used to truncate a table without change permissions
2. [Redirection](#i/o-redirection)
3. **String** , **Integer** or **ASCII** Comparision
4. _\\>,\/>_ word boundary in [Regex](#regex): let word as is.
5. _>() and <()_ is [Process Substitution](#process-substitution)
6. _>|_: force redirection (even if the noclobber [option](#bash-options) is set). This will forcibly overwrite an existing file


[top](#top)
### Character Cases
1. Hat
```bash
  var=firstIsLower
  echo $var  <----- print firstIsLower
  echo ${var} <---- Same above
  echo ${var^} <--- print FirstIsLower
  echo ${var^^} <-- print FIRSTISLOWER
```
2. Comma
```bash
  var=FirstIsUpper
  echo ${var,} <--- print firstIsUpper
  echo ${var,,} <-- print firstisupper
```
3. Declaration
```bash
  declare -l var <--- set var all lowercase as like as echo $var | tr A-Z a-z
  declare -c var <--- set First Upper
  declare -u var <--- set all as Upper as like as echo $var | tr a-z A-Z
```
4. `tr` Command
```bash

```
### Question Mark
1. for test a condition:
```bash
  ((var=condition?echo "True":echo "Fals"))
```
2. Check if variable exist:
```bash
  : ${USER?Err_Massage}
```
3. used in [**Globbing**](#globbing) and [**Regular Experssion**](#regex)

[top](#top)
### Pipe Sign
Passes the **output** of _Previous Command_ as **input** of _Next Command_


[top](#)
### Chain Commands
- _||_:
- _&&_:
- _;_:

[top](#)
### Dash Sign
- single

    1. to specify [default parameter](https://www.tldp.org/LDP/abs/html/parameter-substitution.html#DEFPARAM1):
    ```bash
      var1=1; var2=2; echo ${var3-var2}
    ```
    2. Option Prefix
    3. redirect _to/from_ std**in/out**:
    ```bash
      (cd /Path1/ && tar -cf - .) | (cd /Path2/ && tar -xvf -)
    ```
    4. _previous Working Dorectory_: `cd - `
    5. as Minus
    
- Double
    1. verbatim Options
    2. end of options: `rm -- -badfilename`
    3. to assign **positionall parametes** with _set_ command:
    ```bash
      var= "1 2 3 4 5"
      set -- $var
      echo $3 ------> 3
    ```
[top](#)
### Plus Sign
1. arithmetic operation
2. in [Regular Experssion](#regex)
3. to set on [**Bash Options**](#bash-options)

[top](#)
### Equeal Sign
1.
### Tilda Sign
1. _User Home Dircetory_
2. _~+_ Currecnt Working Directory. it is equeal to **$PWD**
3. _~-_ Previous Working Directory. it is equeal to **cd -**
4. _=~_ coresponed to [Regular Experssion](#regex)

[top](#)
### [Math](https://www.tldp.org/LDP/abs/html/mathc.html) [Signs](https://www.tldp.org/LDP/abs/html/ops.html)
- [_=_](#equeal-sign),[_+_](#plus-sign),[_-_](#dash-sign),[_*_](#star-sign),[_%_](#percent-sign),[_**_](#star-sign), [!](#exclamation-mark)
- _+=_,_*=_,...
- _<<_: bitwise left shift (multiplies by 2 for each shift position)
- _<<=_: left-shift-equal.`let "var <<= 2"` results in var left-shifted 2 bits (multiplied by 4)
- _>>_: bitwise right shift (divides by 2 for each shift position)
- _>>=_: right-shift-equal (inverse of <<=)
- _&_,_&=_: bitwise AND, AND-equal
- _|_,_|=_: bitwise OR, OR-equal
- _~_: bitwise NOT
- _^_, _^=_: bitwise XOR, XOR-equal
- _&&_,_||_: And, Or
- _**,**_: Chaning Command together and only last Operation result return.
```bash
  q=((a=3>4?1:2, 4*8))
  echo $q   $a -------> 32   2
```

[top](#top)
## [I/O Redirection](https://www.tldp.org/LDP/abs/html/io-redirection.html)
there are three default **file descriptor** open in terminal:
  1. stdin:  or **fd0** is _Keyword_ by default.
  2. stdout: or **fd1** is _Screen_  by default.
  3. stderr: or **fd2** is _Screen_  by default.
and _fd3_ to _fd9_ are remained. 
1. `>FileName` : redirect **StdOut** to _FileName_ file
2. `2>FileName`: redirect **StdErr** to _FileName_ file
3. `&> FileName`~`>& FileName`: redirect both **StdOut** and **StdErr** to _FileName_ file
4. `2>1` : redirect **StdErr** to _StdOut_ file descriptor
5. `M>FileName` : redirect **fdM** to _FileName_ file
6. `M>&N` : redirect **fdM** to **fdN** _file descriptor_
7. `<FileName`: redirect **StdIn** from _FileName_ file
8. `<&M`: redirect **StdIn** from **fdM** _file descriptor_
9. `M<>FileName`: Open **FileName** file for reading and writing and assign **fdM** to it
10. `Command<InputFile>OutputFile`
11. `M<&-`,`M>&-`: close **Input**/**Output** file descriptor
```bash
  echo 1234567890 > File    # Write string to "File".
  exec 3<> File             # Open "File" and assign fd 3 to it.
  read -n 4 <&3             # Read only 4 characters.
  echo -n . >&3             # Write a decimal point there.
  exec 3>&-                 # Close fd 3.
  cat File                  # ==> 1234.67890
```
* [here](https://www.tldp.org/LDP/abs/html/redircb.html) you found good example of **loop** _redirection_.
12. [_<<_](https://www.tldp.org/LDP/abs/html/here-docs.html): is _here document_ :
```bash
 cat <<EndFile > NewFile  <----------- the dash sign suppresses tab 
    qqqqqqqq
    wwwwwwww
    eeeeeeee
    EndFile
```
13. [_<<<_](https://www.tldp.org/LDP/abs/html/x17837.html#HERESTRINGSREF): is _here String_:
```bash
  String="This is a string of words."
  read -r -a Words <<< "$String"
  if grep -q "txt" <<< "$VAR" ...
  cat - $file <<<$title > $file.new
  
```
[top](#)
## Bash Options
use set -o option-name or set -option-abbrev.To disable an option within a script, use set +o option-name or set +option-abbrev.

Abbreviation|Name|Effect
---|---|---
-B|brace expansion|Enable brace expansion (default setting = on)
+B|brace expansion|Disable brace expansion
-C|noclobber|Prevent overwriting of files by redirection (may be overridden by >|)
-D|(none)|List double-quoted strings prefixed by $, but do not execute commands in script
-a|allexport|Export all defined variables
-b|notify|Notify when jobs running in background terminate (not of much use in a script)
-c ...|(none)|Read commands from ...
checkjobs|(none)|Informs user of any open jobs upon shell exit. Introduced in version 4 of Bash, and still "experimental." Usage: shopt -s checkjobs (Caution: may hang!)
-e|errexit|Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
-f|noglob|Filename expansion (globbing) disabled
globstar|globbing star-match|Enables the ** globbing operator (version 4+ of Bash). Usage: shopt -s globstar
-i|interactive|Script runs in interactive mode
-n|noexec|Read commands in script, but do not execute them (syntax check)
-o Option-Name|(none)|Invoke the Option-Name option
-o posix|POSIX|Change the behavior of Bash, or invoked script, to conform to POSIX standard.
-o pipefail|pipe failure|Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.
-p|privileged|Script runs as "suid" (caution!)
-r|restricted|Script runs in restricted mode (see Chapter 22).
-s|stdin|Read commands from stdin
-t|(none)|Exit after first command
-u|nounset|Attempt to use undefined variable outputs error message, and forces an exit
-v|verbose|Print each command to stdout before executing it
-x|xtrace|Similar to -v, but expands commands
-|(none)|End of options flag. All other arguments are positional parameters.
--|(none)|Unset positional parameters. If arguments given (-- arg1 arg2), positional parameters set to arguments.

[top](#)
## Keyword Combination
- <kbd>Ctl</kbd>-<kbd>A</kbd>: Moves cursor to beginning of line of text (on the command-line).
- <kbd>Ctl</kbd>-<kbd>B</kbd>: Backspace (nondestructive).
- <kbd>Ctl</kbd>-<kbd>C</kbd>: Break. Terminate a foreground job.
- <kbd>Ctl</kbd>-<kbd>D</kbd>: Log out from a shell (similar to exit).
- <kbd>Ctl</kbd>-<kbd>E</kbd>: Moves cursor to end of line of text (on the command-line).
- <kbd>Ctl</kbd>-<kbd>F</kbd>: Moves cursor forward one character position (on the command-line).
- <kbd>Ctl</kbd>-<kbd>G</kbd>: BEL. On some old-time teletype terminals, this would actually ring a bell. In an xterm it might beep.
- <kbd>Ctl</kbd>-<kbd>H</kbd>: Rubout (destructive backspace). Erases characters the cursor backs over while backspacing.
- <kbd>Ctl</kbd>-<kbd>I</kbd>: Horizontal tab.
- <kbd>Ctl</kbd>-<kbd>J</kbd>: Newline (line feed). In a script, may also be expressed in octal notation -- '\012' or in hexadecimal -- '\x0a'.
- <kbd>Ctl</kbd>-<kbd>K</kbd>: Vertical tab.When typing text on the console or in an xterm window, Ctl-K erases from the character under the cursor to end of line. Within a script, Ctl-K may behave differently, as in Lee Lee Maschmeyer's example, below.
- <kbd>Ctl</kbd>-<kbd>L</kbd>: Formfeed (clear the terminal screen). In a terminal, this has the same effect as the clear command. When sent to a printer, a Ctl-L causes an advance to end of the paper sheet.
- <kbd>Ctl</kbd>-<kbd>M</kbd>: Carriage return.
- <kbd>Ctl</kbd>-<kbd>N</kbd>: Erases a line of text recalled from history buffer [8] (on the command-line).
- <kbd>Ctl</kbd>-<kbd>O</kbd>: Issues a newline (on the command-line).
- <kbd>Ctl</kbd>-<kbd>P</kbd>: Recalls last command from history buffer (on the command-line).
- <kbd>Ctl</kbd>-<kbd>Q</kbd>: Resume (XON).This resumes stdin in a terminal.
- <kbd>Ctl</kbd>-<kbd>R</kbd>: Backwards search for text in history buffer (on the command-line).
- <kbd>Ctl</kbd>-<kbd>S</kbd>: Suspend (XOFF).This freezes stdin in a terminal. (Use Ctl-Q to restore input.)
- <kbd>Ctl</kbd>-<kbd>T</kbd>: Reverses the position of the character the cursor is on with the previous character (on the command-line).
- <kbd>Ctl</kbd>-<kbd>U</kbd>: Erase a line of input, from the cursor backward to beginning of line. In some settings, Ctl-U erases the entire line of input, regardless of cursor position.
- <kbd>Ctl</kbd>-<kbd>V</kbd>: When inputting text, Ctl-V permits inserting control characters. For example, the following two are equivalent.Ctl-V is primarily useful from within a text editor.
- <kbd>Ctl</kbd>-<kbd>W</kbd>: When typing text on the console or in an xterm window, Ctl-W erases from the character under the cursor backwards to the first instance of whitespace. In some settings, Ctl-W erases backwards to first non-alphanumeric character.
- <kbd>Ctl</kbd>-<kbd>X</kbd>: In certain word processing programs, Cuts highlighted text and copies to clipboard.
- <kbd>Ctl</kbd>-<kbd>Y</kbd>: Pastes back text previously erased (with Ctl-U or Ctl-W).
- <kbd>Ctl</kbd>-<kbd>Z</kbd>: Pauses a foreground job.Substitute operation in certain word processing applications.EOF (end-of-file) character in the MSDOS filesystem.
- EOF (end-of-file). This also terminates input from stdin.When typing text on the console or in an xterm window, Ctl-D erases the character under the cursor. When there are no characters present, Ctl-D logs out of the session, as expected. In an xterm window, this has the effect of closing the window.

[top](#)
# [**GETOPTS Powerfull Variable in Bash**](https://www.tldp.org/LDP/abs/html/internal.html#GETOPTSX)
this availabe user to define _Options_ for his **Scripts**.
there are two variable:
  1. $OPTIND: containd and keep Option's Index
  2. $OPTARG: containd and keep Option's Passed Parameter if has.
```bash
  while getopts ":asd:ewx:ty" Option   <----------- d: and x: meant they have passed parameter
  do
    case $Option in
      a) echo "a Option passed. Index is $OPTIND"
      s) echo "s Option passed. Index is $OPTIND"
      d) echo "d Option passed. Index is $OPTIND and the passed Parameter is $OPTARG"
      ...
  done
  shift $(($OPTIND-1))  <--------------- now, $1 in first Positional Parameter and so.
```

[top](#)
# Bash Processes
- [Bash Subshells](#bash-subshells)

## [Bash Subshells](https://www.tldp.org/LDP/abs/html/subshells.html#SUBSHELLSREF)
_A subshell_ is **a child process launched by a shell (or shell script).**
Running a shell script launches a new process, a subshell.
When a command or the shell itself initiates (or spawns) a new subprocess to carry out a task, this is called forking. This new process is the child, and the process that forked it off is the parent. While the child process is doing its work, the parent process is still executing.

## ProcessID Varibales
- **$BASHPID**: Process ID of the current instance of Bash. This is not the same as the $$ variable, but it often gives the same result.
- **$BASH_SUBSHELL**: A variable indicating the subshell level. This is a new addition to Bash, version 3.
- **$$**:
```bash
  #!/bin/bash4
  
  echo "\$\$ outside of subshell = $$"                              # 9602
  echo "\$BASH_SUBSHELL  outside of subshell = $BASH_SUBSHELL"      # 0
  echo "\$BASHPID outside of subshell = $BASHPID"                   # 9602
  
  echo
  
  ( echo "\$\$ inside of subshell = $$"                             # 9602
    echo "\$BASH_SUBSHELL inside of subshell = $BASH_SUBSHELL"      # 1
    echo "\$BASHPID inside of subshell = $BASHPID" )                # 9603
    # Note that $$ returns PID of parent process.
```

[top](#)
## [Process Substitution](https://www.tldp.org/LDP/abs/html/process-sub.html)
[top](#)
# Bash Version4

[top](#)
# Jobs
### \& :
### fg :
### bg :
### disown :
  `run command`,<kbd>Ctrl</kbd><kbd>Z</kbd>,`disown -h Job-ID`
[top](#top)
# Bash Command line tools
- [Command Line Web Browsing](#command-line-web-browsing)

## Command Line Web Browsing
- [links and links2](#links-and-links2)
- [lynx](#lynks)
- [youtube-dl](#youtube-dl)
- [Wget](BashCommands.md/#wget)
- [w3m](#w3m)
- [Curl](BashCommands.md/#curl)

[top](#)
### links and links2
### lynx
```bash
  lynx -dump http://www.xyz23.com/file01.html >$SAVEFILE
```
### youtube-dl
- make ready: 
```bash
  wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
  chmod a+rx /usr/local/bin/youtube-dl
```
- [Using](https://github.com/rg3/youtube-dl/blob/master/README.md#readme):
```bash
  youtube-dl https://www.youtube.com/.......... <------------------------------- Download a video
  youtube-dl --yes-playlist https://www.youtube.com/.......... <---------------- Download playlist if refer to Playlist
  youtube-dl https://www.youtube.com/.......... <------------------------------- Download a video
  youtube-dl -U <--------------------------------------------------------------- Update
```

### w3m

[top](#)
# Users
* ### Change username:
 - becare first find OldName user and group ID.
```vim
  usermod -l NewName OldName
  groupmod -n NewName oldName
  usermod -d /home/NewName -m NewName
  find / -group OldGroupID -exec chgrp -h NewName {} \;
  find / -user OldUserID -exec chown -h Newname {} \;
```
* ### useradd
```vim
  -e Account Expir Date
  -f Password Expire
  -M without Home Directory
  -d Specified Home Directory
  -N without Group
  -s specified shell
  -L without loggin permission
```
* ### usermod
- Change Primary Group:
```vim
  usermod -g GroupName UserName
```
- Change UserId:
```vim
  usermod -u UserId UserName
```

[top](#top)

# IPv6
* ### disabling IPv6
```vim
  vim /etc/sysctl.conf
  net.ipv6.conf.all.disable_ipv6 = 1
  net.ipv6.conf.default.disable_ipv6 = 1
  sysctl -p
```

[top](#top)

# VPN Routes
* ### [coonect client to server which will connected to vpn:](http://unix.stackexchange.com/questions/237460/ssh-into-a-server-which-is-connected-to-a-vpn-service)
 + Public IP is 50.1.2.3
 +  Public IP Subnet is 50.1.2.0/24
 +  Default Gateway is x.x.x.1
 +  eth0 is device to gateway
```vim
  ip rule add table 128 from 50.1.2.3
  ip route add table 128 to 50.1.2.0/24 dev eth0
  ip route add table 128 default via x.x.x.1
```
* ### Scans
```vim
  IPTABLES -A INPUT -p tcp -i eth0 -m state --state NEW -m recent --set
  IPTABLES -A INPUT -p tcp -i eth0 -m state --state NEW -m recent --update --seconds 30 --hitcount 10 -j DROP
```

[top](#top)
# Scripting
- [Logging](#logging)
- [Variable](#variable)
- [Input Prameters](#input-parameters)
- [Loop](#loop)
- [Conditional](#conditional)

[top](#top)
### Logging
- generate log of output:
```vim
  exec 3>&1 4>&2
  trap 'exec 2>&4 1>&3' 0 1 2 3
  exec 1>log.out 2>&1
```

[top](#top)
### Varisable
- define variable: `Var=Value` <-- be care _No Space_
- defualt value for variable: `[ "$Var" == "" ] && Var=DefaultValue`
- Variable dealing:
  - assign empty variable: simply use `VariableName=`
  - incrementally assign: `VariableName="$VariableName ..."` <-- be care about <kbd>Space</kbd>


[top](#top)
### Input Parameters
- input Parameter : `Par1=$1` , ... <-- be care _No Space_
- input arguments:
 - `$@` : a list of all input arguments
 - `$#` : number of input arguments


[top](#top)
### Array
- String to array: `IFS='sep' read -ra ListName <<< $1`
- Length of Array: `${#ArrayName[@]}`
  
[top](#top)
### Loop
- Loop:
  - for:
  ```sh
    for 1 in {1..100};do ...;done
    for 1 in {1..100..10};do ...;done
    for ((i=0;i<No.;i++));do ...;done
  ```
[top](#top)
### Conditional
- if : structure is:
```vim
  if [ cluase ] 
  then
    Block
  elif [ Cluase ]
  then
    Block
  else
    Block
  fi
```
  1. Switchs:
  A Taxonomy of Data Science
  
Primary|Meaning
---|---  
[ -a FILE ]	|True if FILE exists.
[ -b FILE ]	|True if FILE exists and is a block-special file.
[ -c FILE ]	|True if FILE exists and is a character-special file.
[ -d FILE ]	|True if FILE exists and is a directory.
[ -e FILE ]	|True if FILE exists.
[ -f FILE ]	|True if FILE exists and is a regular file.
[ -g FILE ]	|True if FILE exists and its SGID bit is set.
[ -h FILE ]	|True if FILE exists and is a symbolic link.
[ -k FILE ]	|True if FILE exists and its sticky bit is set.
[ -p FILE ]	|True if FILE exists and is a named pipe (FIFO).
[ -r FILE ]	|True if FILE exists and is readable.
[ -s FILE ]	|True if FILE exists and has a size greater than zero.
[ -t FD ]	|True if file descriptor FD is open and refers to a terminal.
[ -u FILE ]	|True if FILE exists and its SUID (set user ID) bit is set.
[ -w FILE ]	|True if FILE exists and is writable.
[ -x FILE ]	|True if FILE exists and is executable.
[ -O FILE ]	|True if FILE exists and is owned by the effective user ID.
[ -G FILE ]	|True if FILE exists and is owned by the effective group ID.
[ -L FILE ]	|True if FILE exists and is a symbolic link.
[ -N FILE ]	|True if FILE exists and has been modified since it was last read.
[ -S FILE ]	|True if FILE exists and is a socket.
[ FILE1 -nt FILE2 ]	|True if FILE1 has been changed more recently than FILE2, or if FILE1 exists and FILE2 does not.
[ FILE1 -ot FILE2 ]	|True if FILE1 is older than FILE2, or is FILE2 exists and FILE1 does not.
[ FILE1 -ef FILE2 ]	|True if FILE1 and FILE2 refer to the same device and inode numbers.
[ -o OPTIONNAME ]	|True if shell option "OPTIONNAME" is enabled.
[ -z STRING ]	|True of the length if "STRING" is zero.
[ -n STRING ] or [ STRING ]	|True if the length of "STRING" is non-zero.
[ STRING1 == STRING2 ] |True if the strings are equal. "=" may be used instead of "==" for strict POSIX compliance.
[ STRING1 != STRING2 ] |True if the strings are not equal.
[ STRING1 < STRING2 ] |True if "STRING1" sorts before "STRING2" lexicographically in the current locale.
[ STRING1 > STRING2 ] |True if "STRING1" sorts after "STRING2" lexicographically in the current locale.
[ ARG1 OP ARG2 ] |"OP" is one of -eq, -ne, -lt, -le, -gt or -ge. These arithmetic binary operators return true if "ARG1" is equal to, not equal to,<br/>less than, less than or equal to, greater than, or greater than or equal to "ARG2", respectively."ARG1" and "ARG2" are integers.

[top](#top)

  2. Combining expressions

Operation	|Effect|
---|---
[ ! EXPR ]	|True if EXPR is false.|
[ ( EXPR ) ]	|Returns the value of EXPR. This may be used to override the normal precedence of operators.|
[ EXPR1 -a EXPR2 ]	|True if both EXPR1 and EXPR2 are true.|
[ EXPR1 -o EXPR2 ]	|True if either EXPR1 or EXPR2 is true.|

[top](#top)

# Text Processing
## REGEX
- [Regex Table](#regex-table)
- [POSIX character set](#posix-character-set)

### Regex Table
__First__ [this Site](https://regex101.com/) is very A Taxonomy of Data Sciencegreate for Online REgex Testing.

Character|Meaning|Example
---|---|---
\*|Match zero, one or more of the previous|[Ah\*](#Bash.md) _matches_ "Ahhhhh" or "A"
?|Match zero or one of the previous but Optional|[Ah?](#Bash.md) _matches_ "Al" or "Ah"
\+|Match one or more of the previous |[Ah+](#Bash.md) _matches_ "Ah" or "Ahhh" but not "A"                                       
\\ |Used to escape a special character|[Hungry\\?](#Bash.md) _matches_ "Hungry?"                                                   
\.|Wildcard char,matches any character|[do.\*](#Bash.md) _matches_ "dog", "door", "dot", etc.                                     
( )|Group characters and preserver sequences|(abc) _mathces_ [abc](#Bash.md) asdf bwefd cwef esr[abc](#Bash.md)ftuj.                                                                             
[ ]|Matches a range of characters|[[cbf]ar](#Bash.md) _matches_ "car", "bar", or "far"<br/>[0-9]+ matches any positive integer<br/>[[a-zA-Z]](#Bash.md) _matches_ ascii letters a-z (UP and LO case)<br/>[[^0-9]](#Bash.md) _matches_ any character not 0-9.                                        
\||Matche previous OR next<br/>character/group|[(Mon)\|(Tues)day](#Bash.md) _matches_ "Monday" or "Tuesday"                                
{ }|Matches a specified number of<br/>occurrences|[[0-9]{3}](#Bash.md) _matches_ "315" but not "31"<br/>[[0-9]{2,4}](#Bash.md) _matches_ "12", "123", and "1234"<br/>[[0-9]{2,}](#Bash.md) _matches_ "1234567..."                          
^|Beginning of a string Or within a<br/>character range [] negation|[^http](#Bash.md) _matches_ strings that begin with http,such as a url<br/>[[^0-9]](#Bash.md) _matches_ any character not 0-9.                   
$|End of a string.|[ing$](#Bash.md) _matches_ "exciting" but not "ingenious"
?=|Positive Lookahead<br/>When you look a pattern that followed by some patterns|[(\w\+\|\d\+)his\s(?=Followed)](#Bash.md) _matches_ with [This](#) Followed but This one is not.
?!|Negative Lookahead<br/> Opposite of abive|[(\w\+\|\d\+)his\s(?=Followed)](#Bash.md) _matches_ with This Followed but [This](#) one is not.
?<=|Positive Lookbehind<br/> Like Positive Lookahead but we looking for second pattern|(?<=(\w{1}\|\d{1})his)\s(Followed) _matches_ This [Followed](#Bash.md) but " Followed " word not and this.
?<!|Negative Lookbehind<br/> Like Positive Lookahead but we looking for second pattern|(?<!(\w{1}\|\d{1})his)\s(Followed) _mathches_ This Followed but " [Followed](#Bash.md) " word not and this .
\\<,\/>|to specify a word as is|\\<word\/> told only _word_ not _Word_ or _wordd_
=~|Pattern Matches|if [[ "$input" =~ "[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]" ]]

__Shorthand Character Sets__

Shorthand|Description
---|---
.|Any character except new line
\\w|Matches alphanumeric characters: [a-zA-Z0-9_]
\\W|Matches non-alphanumeric characters: [^\w]
\\d|Matches digit: [0-9]
\\D|Matches non-digit: [^\d]
\\s|Matches whitespace character: [\t\n\f\r\p{Z}]
\\S|Matches non-whitespace character: [^\s]

__Chanie Patterns followed__

[top](#top)

### POSIX character set
in newer we could use [:alpha:] instead of [a-z], the advantage is that this match nationalization chars.

Character Group|Meaning
---|---
[:alnum:]|Alphanumeric
[:cntrl:]|Control Character
[:lower:]|Lower case character
[:space:]|Whitespace
[:alpha:]|Alphabetic
[[:alpha:]]|[a-zA-Z]]
[:digit:]|Digit
[:print:]|Printable character
[:upper:]|Upper Case Character
[:blank:]|whitespace, tabs, etc.
[:graph:]|Printable and visible characters
[:punct:]|Punctuation
[:xdigit:]|Extended Digit

[top](#top)

## AWK
- Kill Prossecc:
```vim
    kill -9 `ps faux | grep Name | awk -F\  '{print $2}''`
```
- Calculate total ram using with App:
```vim
  ps faux|grep App| awk '{Sum=+$6} END {print Sum/1024/1024 "G"}'
```

## CUT

- return carachters No1 to No2 for each line:
```vim
  cut -cNo1-No2 File
```
- return column No while Seperator is Col seperator:
```vim
  cut -d'Seperator' -fColNo1-ColNo2,ColNo3
```
- show all cols except specified fields:
```vim
  cut -d'Seperator' --complement -s -fNos
```
- Change delemiter out put to _Return_:
```vim
  cut ... --output-delimiter=$'\n'
```

[top](#top)
## SED
- replace a word in file:
```vim
  sed -i 's/OldWord/NewWord/g' FileName
  sed -i.bak ...   <- replace and get backup of original files
```
- replace a word in Files
```vim
  find ./ type f -ecex sed -i 's/OldWord/NewWord/g' {} \;
```
- No.th Line:
```vim
  sed -n 'No.p' or sed 'No.-1q;d' FileName
```
- Multiple Line:
```vala
  sed -n 'No1.,No2p' FileName
```
- Remove (a) Lines:
 ```vim
   sed '/Pattern/d' FileName # Containing a pattern
   sed 'No1..No2d' FileName
 ```
  - MultiLine
   ```vim
     sed '/Pattern/, +5 d' FileName
   ```
- Change case
```bash
  sed 's/[A-Z]/\L&/g' *
  sed 's/[a-z]/\U&/g' *
  sed -r 's/\b(.)/\u\1/g' *
```
- Change Newline
```bash
  sed -z 's/\n/,/g' *               # with zero option
  sed ':a;N;$!ba;s/\n/,/g' *        # with label
  sed 'H;1h;$!d;x;y/\n/g' *         # with pattern space
  sed 'H;1h;${g;s/\n/,/g}' *
```
[top](#top)
## Sort

[top](#top)
## Uniq

[top](#top)
## Head and Tail
```vim
  head -n No. File
  tail tail -n No. File
```
[top](#top)
## Grep
```vim
  grep -n ... # show line number of match
  grep [-e|-E] "Pattern" ... # Search in file for BRE|ERE pattern
  grep -w "Word" ... # Find whole of word
  grep [-A|-B|-C] No. .... # Print No. Lines After|Before Match|both of before
  grep -c ... # Count of matvh
  grep -r -v ... # recursively search with reverse search
  grep -l ... # display only file name that match
  grep -o ... # showing only matched string
```
  - In basic regular expressions(BRE) the meta-characters ?,+,{,|,(,) lose their special meaning; 
    instead use the backslashed versions \\?,\\+,\\{,\\|,\\(,\\),but not in extended regular experssion(ERE)

[top](#top)
## Wc
```vim
  wc -l File # Number Lines
  wc -w File # NUmber Words
  wc -m File # NUmber Characters
  wc -L File # lengest line width 
```
[top](#top)
## Tr
it is useful to make nice the output of processing:
```vim
  tr -d [:punct:] <inFileName > outFileName
  tr [:upper:] [:lower:] ...
```  

## Paste
Join two file
- simple join: `paste f1 f2`
- join with row number: `paste number f1 f2`
- join with separator: `paste -d'|' number f{1,2}`
- join with multiple separator: `paste -d'|,' f1 f2`
- serial join: `paste -s f1 f2`
- join one file lines: it depends to how number of hypens are
  below join each three lines
```bash
  cat f1 | paste - - -
```

[top](#top)

## Join
1. -a FILENUM : Also, print unpairable lines from file FILENUM, where FILENUM is 1 or 2, corresponding to FILE1 or FILE2.
2. -e EMPTY : Replace missing input fields with EMPTY.
3. -i - -ignore-case : Ignore differences in case when comparing fields.
4. -j FIELD : Equivalent to "-1 FIELD -2 FIELD".
5. -o FORMAT : Obey FORMAT while constructing output line.
6. -t CHAR : Use CHAR as input and output field separator.
7. -v FILENUM : Like -a FILENUM, but suppress joined output lines.
8. -1 FIELD : Join on this FIELD of file 1.
9. -2 FIELD : Join on this FIELD of file 2.
10. - -check-order : Check that the input is correctly sorted, even if all input lines are pairable.
11. - -nocheck-order : Do not check that the input is correctly sorted.
12. - -help : Display a help message and exit.
13. - -version : Display version information and exit.
[top](#top)

##Comm
understanding the options
1. -1 :suppress first column(lines unique to first file).
2. -2 :suppress second column(lines unique to second file).
3. -3 :suppress third column(lines common to both files).
4. – -check-order :check that the input is correctly sorted, even if all input lines are pairable.
5. – -nocheck-order :do not check that the input is correctly sorted.
6. – -output-delimiter=STR :separate columns with string STR
7. – -help :display a help message, and exit.
8. – -version :output version information, and exit.

[top](#top)
## Diff
explain the symbols
|Symbol|Meaning|
|---|---|
|A|Add|
|C|Change|
|D|Delete|
|#|Line numbers|
|---|Separates Files in Output|
|<|File 1|
|>|File 2|

- if run `diff` command with _-c_ option, symbols would be changed:
|Symbol|Meaning|
|---|---|
|+|Add|
|!|Change|
|-|Delete|
|***|File 1|
|---|File 2|
- colorized: `diff --color f1 f2`
- minimal output: `diff --minimal f1 f2`
- to quit:  `diff -q f1 f2`



[top](#top)
# Unicode
- [Convert to Ascii](#convert-to-ascii)
- [Convert to each other](#convert-to-each-other)

### Convert to Ascii
this command will convert FIn to FOut from FirstCoding to ascii:
```vim
  native2ascii -encoding FiratCoding -reverse FIn FOut
```
[top](#top)
# ulimit
[this](https://serverfault.com/a/487641/209634)is a good explanation

