<div dir="rtl" align="right">بنام خدا</div>

***
> Whitespace == :Space:Tab:Line Feed:Carriage Return=> $'\x20'$'\x09'$'\x0A'$'\x0D' respectively
***

###### Bash Commands
- [Internal Bash Command](#internal-bash-command)
  - [I/O](#io)
  - [File System](#file-system)
  - [Script Behaviour](#script-behaviour)
    - [Source or .](#source-or-.)
    - [Exit](#exit)
    - [Exec](#exec)
    - [shopt](#shopt)
    - [caller](#caller)
  - [Commands](#commands)
- [External Bash Command](#external-bash-command)
  - [Basic Commands](#basic-commands)
  - [Complex Commands](#complex-commands)
  - [Archiving and Compressioning](#archiving-and-compressioning)
  - [File Information](#file-information)
  - [Comparison](#comparison)
  - [Utilities](#utilities)
  - [Commutication Commands](#commutication-commands)



[top](#)
# Internal Bash Command
- [I/O](#io)
- [File System](#file-system)
- [Script Behaviour](#script-behaviour)
- [Source or .](#source-or-.)
- [Exit](#exit)
- [Exec](#exec)
- [shopt](#shopt)
- [caller](#caller)
- [Commands](#commands)

[top](#)
## Internal Bash Command
### I/O
- [echo](#echo)
- [printf](#printf)
- [read](#read)

[top](#internal-bash-command)
#### echo
```bash
  echo Hello
  a=`echo "Value" | tr a-z A-Z
  echo `ll` ---------------------------------> suppress NewLine
  echo "Preserve \n Newline sign" -----------> preserve NewLine
  echo "Preserve " $"\n" "Newline sign" -----> preserve NewLine
  echo $var ~ ${var} ~ "$var" ~ "${var}" !~ '$var'
```
[top](#io)
#### printf
enhanced **Echo**: `printf format-string parameter`
see [this](https://alvinalexander.com/programming/printf-format-cheat-sheet) for reference.
#### read
"Reads" the value of a variable from stdin, that is, interactively fetches input from the keyboard. The -a option lets read get array variables.
* _A read without an associated variable assigns its input to the dedicated variable $REPLY._
- _-r_: to catche scape character use .
- _-s_ option means do not echo input.
- _-n_ N option means accept only N characters of input.option to read also allows detection of the arrow keys and certain of the other unusual keys.will not detect the ENTER (newline) key.
- _-p_ option means echo the following prompt before reading input.
- _-t_ option to read permits timed input 
- _-u_ option takes the file descriptor of the target file.
```bash
  read -s -n1 -p "Hit a key " keypress
  TIMELIMIT=4         # 4 seconds
  read -t $TIMELIMIT variable
  while read line
  do
    echo "$line"
  done <data-file
  cat file1 file2 | while read line; do echo $line; done <--------------- Worked
  echo "file1 file2" | while read line; do echo $line; done <--------------- Not Worked
  shopt -s lastpipe; echo "file1 file2" | while read line; do echo $line; done <--------------- Worked
```
[top](#io)
### File System
- [cd](#cd)
- [pwd](#pwd)
- [dirs,popd,pushd](#dirspopdpushd)

[top](#io)
#### cd
#### pwd
#### dirs,popd,pushd
[top](#file-system)
### Script Behavior
#### _source_ or _**.**_
like _#include_ command in **C**, import codes into script or when run in terminal will execute _file_.
#### [_exit_](https://www.tldp.org/LDP/abs/html/exitcodes.html#EXITCODESREF)
unconditionally terminates the script. 
- If a script terminates with an exit lacking an argument, the exit status of the script is the exit status of the last command executed in the script, not counting the exit. This is equivalent to an exit $?.
- An exit command may also be used to terminate a subshell.

Exit Code Number|Meaning|Example|Comments
---|---|---|---
1|Catchall for general errors|let "var1 = 1/0"|Miscellaneous errors, such as "divide by zero" and other impermissible operations
2|Misuse of shell builtins (according to Bash documentation)|empty_function() {}|Missing keyword or command, or permission problem (and diff return code on a failed binary file comparison).
126|Command invoked cannot execute|/dev/null|Permission problem or command is not an executable
127|"command not found"|illegal_command|Possible problem with $PATH or a typo
128|Invalid argument to exit|exit 3.14159|exit takes only integer args in the range 0 - 255 (see first footnote)
128+n|Fatal error signal "n"|kill -9 $PPID of script|$? returns 137 (128 + 9)
130|Script terminated by Control-C|Ctl-C|Control-C is fatal error signal 2, (130 = 128 + 2, see above)
255*|Exit status out of range|exit -1|exit takes only integer args in the range 0 - 255
[top](#file-system)
#### _exec_
- not forked off parent process and replace currenct process with command.
- serves to reassign file descriptors.	For example, exec <zzz-file replaces stdin with the file zzz-file.
#### _shopt_
This command permits changing shell options on the fly
#### caller
Putting a caller command inside a function echoes to stdout information about the caller of that function. like [line] 8 FuncNameThatCall ScriptName

[top](#file-system)
### Commands
- _true_: return 0 exit status
- _flase_: returns an unsucceful exit result
- _type_: returns Command Identifies
- _hash [cmds]_: Records the path name of specified commands in the shell hash table, so the shell or script will not need to search the $PATH on subsequent calls to those commands. 
  - When hash is called with no arguments, it simply lists the commands that have been hashed.
  - The -r option resets the hash table.
- _bind_: The bind builtin displays or modifies readline [9] key bindings.
- _help_: Gets a short usage summary of a shell builtin. This is the counterpart to whatis, but for builtins.

[top](#file-system)
# External Bash Command
- [Basic Commands](#basic-commands)
- [Complex Commands](#complex-commands)
- [Archiving and Compressioning](#archiving-and-compressioning)
- [File Information](#file-information)
- [Comparison](#comparison)
- [Utilities](#utilities)
- [Commutication Commands](#communication-commands)



[top](#)
## Basic Commands
_ls_,_cat_,_tac_,_rev_,_mv_,_rm_,_rmdir_,_mkdir_,_mv_,_cp_,_chmod_,[_chattr_](FilesAndPartitioning.md#file-attributes),_ln_,_info_,_man_

[top](#external-bash-command)
## Complex Commands
- [Find](#find)
- [Xargs](#xargs)
- [Expr](Bash.md#variables-definition)
- [Date](#date)


[top](#external-bash-command)
### Find
- [Execute Command](#execute-command)
- [find by Time](#find-by-time)
- [find by User](#find-by-user)
- [find by Users and Permissions](#find-by-users-and-permissions)


[top](#complex-commands)
#### Execute Command
1. Simple Sample:
```bash
  find . -iname "cpp" -type f -exec cp {} /home/root/Bkp/ \;
  find . -name "sample*.py" -type f -exec grep -B5 -A8 'function' {} \;
```
2. find in many Path
```bash
  find multiple paths -name "Pattern" -type Type  -not -name "Pattern" -exec SomeCommands {} SomeCommands \;
```
3. find with specefic type:
   - b:block (buffered) special
   - c:character (unbuffered) special
   - d:directory
   - p:named pipe (FIFO)
   - f:regular file
   - l:symbolic link; this is never true if the -L option or the -follow option is in effect, unless the symbolic link is broken.  If you want to search  for
        symbolic links when -L is in effect, use -xtype.
   - s:socket
   - D:door (Solaris)
4. find with multiple name: 
```bash
  find ... -type f \( -name "\*.cpp" -o -name "\*.csv" \)
```
5. find and tar:
```bash
  find . -type f \( -name "*.py" \) print0 | xargs -0 tar cvf myPyFile.tar  <--print0 handle space in file name.
```
[top](#find)
#### find by Time
1. The main way:
```bash
  touch --date "2017-01-01" start
  touch --date "2018-01-01" end
  find /Path/2/Files -type f -newer start -not -newer end
  find /Path/2/Files -type f -newer start ! -newer end
```
2. *newer<kbd>_</kbd>* Options:
   - a:The access time of the file reference
   - B:The birth time of the file reference
   - c:The inode status change time of reference
   - m:The modification time of the file reference
   - t:reference is interpreted directly as a time
```bash
  find . -type f -newermt 2017-01-01 ! -newermt 2018-01-01 -print0
```
3. *<kbd>_</kbd>time* and *<kbd>_</kbd>min*:
    - _atime_: File  was last accessed n*24 hours ago,e.g. 
    - _-atime +1_ files which accessed at leat 2 days ago.
    - _cmin_ : File's status was last changed _n_ minutes ago.(like chmod)
    - _mmin_ : File's modified was last changed _n_ minutes ago.
    - _mtime_: File's modified was last changed _n_ hours ago.
```bash
  find . -mtime 1  <--midified exactly 24 hours
  find . -mtime -5 <-- 5 days ago
```
[top](#find)
#### find by Users and Permissions
1. find not a User as Owner:
```bash
  find /Path ! -user UserName
```
2. find with permission:
```bash
find . -perm /220
find . -perm /u+w,g+w
find . -perm /u=w,g=w
find . -perm -220
find . -perm -g+w,u+w
```  

[top](#)
### Xargs
The default command for xargs is echo. This means that input piped to xargs may have linefeeds and other whitespace characters stripped out.
- _-n N_: it is useful option, limit **N** number output each time.
```bash
  echo `ls` | xargs -n 2 echo
```
> file1 file2
>
> file3 file4

- _-0_: it is usefull when arguments containing **WhiteSpace** or **Quotes**. mut used with _find -print0_ and _grep -lZ_.
- _-P_: it means run in multiple process and usefull for multicore machine.
- _-t_: print command to output.


[top](#complex-commands)
### Date
- _**Date**_ command:
main sample command is `date +Format`

Format option|Part of Date|Desciption|Example Output
---|---|---|---
date +%a|Weekday|Name of weekday in short (like Sun, Mon, Tue, Wed, Thu, Fri, Sat)|Mon
date +%A|Weekday|Name of weekday in full (like Sunday, Monday, Tuesday)|Monday
date +%b|Month|Name of Month in short (like Jan, Feb, Mar )|Jan
date +%B|Month|Month name in full short (like January, February)|January
date +%d|Day|Day of month (e.g., 01)|04
date +%D|MM/DD/YY|Current Date; shown in MM/DD/YY|02/18/18
date +%F|YYYY-MM-DDDate; shown in YYYY-MM-DD|2018-01-19
date +%H|Hour|Hour in 24-hour clock format|18
date +%I|Hour|Hour in 12-hour clock format|10
date +%j|Day|Day of year (001..366)|152
date +%m|Month|Number of month (01..12) (01 is January)|05
date +%M|Minutes|Minutes (00..59)|52
date +%S|Seconds|Seconds (00..59)|18
date +%N|Nanoseconds|Nanoseconds (000000000..999999999)|300231695
date +%T|HH:MM:SS|Time as HH:MM:SS (Hours in 24 Format)|18:55:42
date +%u|Day of WeekDay of week (1..7); 1 is Monday|7
date +%U|Week|Displays week number of year, with Sunday as first day of week (00..53)|23
date +%Y|Year|Displays full year i.e. YYYY|2018
date +%Z|Timezone|Time zone abbreviation (Ex: IST, GMT)|IST

[top](#complex-commands)
- _**zdump**_ command. the available name are in _/usr/share/zoneinfo_
```bash
  zdump Iran
  zdump Jamaica
```
- _**time**_ to calculate statistics of command .
- _**touch**_ command useful for updating file time _acess/modified_.
- _**cal**_ command will print neatly formatted of month.
- _**sleep** and **usleep**_ commands
- _**clock** and **hwclock**_ commands 

[top](#complex-commands)
## Archiving And Compressioning
- [Archiving](#archiving)
  - [tar](#tar)
  - [ar](#ar)
  - [shar](#shar)
  - [rpm](#rpm)
  - [cpio](#cpio)
    - [rpm2cpio](#rpm2cpio)
  - [pax](#pax)
- [Compressioning](#compressioning)
  - [gzip and gunzip](#gzip-and-gunzip)
  - [bzip2 and bunzip2](#bzip2-and-bunzip2)
  - [compress and decompress](#compress-and-decompress)
  - [sq](#sq)
  - [zip and unzip](#zip-and-unzip)
  - [lzma , unlmza and lzcat](#lzma-unlzma-and-lzcat)
  - [xz , unxz and xzcat](#xz-unxz-and-xzcat)
  - [inarc , unarj and unrar](#unarc-unarj-and-unrar)
- [File Information](#file-information)

[top](#external-bash-command)
#### Archiving
- ###### _tar_: is unix standard _archive_ utilities.
a list of some options:

Option|Description
---|---
-c| Create Archive
-x| Extract Archive
-r| Append file to existing Archive
-A| Append _tar_ file to existing Archive
-u| like _-c_ and _-r_, it append newer file of existing file to the end of archive and keep both
-t| list files existing in Archive file
--delete| delete file from Archive
-d| Compress with specified fileSystem
-z| _gzip_ compression
-j| _bzip2_ compression

[top](#archiving-and-compressioning)
- ###### _ar_: is also archive utils and main proprties are:
    1. with _tar_ is this only for files and not directories. 
    2. other one , it keeps files permissions.
    3. more usefull for binary files
- _shar_: it is a self_exctraction_archive command and usefull for shall files.
- _rpm_: is _RedHat Package Manager_ tool.
- ###### _cpio_: 
    is Archive command too and supplanted with _tar/gzip_ commands. actually it has its usefull yet becuse faster than supplanted tools.
   - ###### rpm2cpio:
     extract _cpio_ archive from _rpm_ archive.
- ###### _pax_: is a new tool that introduce to replace _tar_ and _cpio_ .


[top](#archiving-and-compressioning)
#### Compressioning
- ###### _gzip_ and _gunzip_: 
  are **UNIX** standard _compression_ and _decompression_ tools replace [compress and decompress](#compress-and-decompress) tools.
  * _-c_ option sends output to **stdout** and usefull for _piping_.
  * _gzip -d_ is equeavalent of _gunzip_ command.
  * _gzip -cd_ ~ _zcat_. **zcat** is _cat_ command works on _compress_ files.
- ###### _bzip2_ and _bunzip2_: 
  is more efficient of _gzip_ but slower than.
- ###### _compress_ and _decompress:
  are oldest unix Compression tools.
- ###### _sq_:
  is another _compressing_ tool that only works on **Ascii** sortrd word lists.
- ###### _zip_ and _unzip_:
- ###### _lzma_ , _unlzma_ and _lzcat_:
  syntax like _gzip_ and in [7zip](https://www.7-zip.org/sdk.html) you find more details.
- ###### _xz_ , _unxz_ and _xzcat_: 
  a newly tool.
- ###### _unarc_ , _unarj_ and _unrar_: 
  unix tools compatible with **DOS** .

[top](#archiving-and-compressioning)
## File Information
- [file](#file)
- [Which, Whereis and Whatis](#which-whereis-and-whatis)
- _vdir_: similar to `ls -l`
- _locate_ and _slocate_: searching file in `updatedb` database._slocate_ is a **locate secure** version.
- [File Access List](#file-access-list)
- _readlink_: like `ls -l` on linked file.
- _strings_: prints _readable characters_ insight's **binary** or files like that.


[top](#external-bash-command)
### file
this utitliy use [Magic Number](https://www.tldp.org/LDP/abs/html/sha-bang.html#MAGNUMREF) to find _file type_ in /usr/{share,lib}/magic or /etc/magic.
* in _bash Script_ should use with **-f** option
* **-z** option usefull on _Compress_ files
#### Which, Whereis and Whatis
**Which**, Whereis and Whatis.
**Whereis** Similar to which, above, whereis command gives the full path to "command".
**Whatis** is usefull to find system commands and files configuration and search in _whatis_ database.
### File Access List
the package is acl : `yum -y install acl`
1. get the Access List:
```vim
  getfacl /Path
```
[top](#file-information)
the Output like
***
> getfacl: Removing leading '/' from absolute path names \\ \
> \# file: Path \
> \# owner: root \
> \# group: root \
> user::rwx \
> user:cent:r-- \
> group::--- \
> mask::r-- \
> other::--- \
> default:user::rwx \
> default:user:cent:r-x \
> default:group::--- \
> default\:\mask\::r-x \
> default:other::--- 
***

[top](#file-information)
2. Set or Remove Acl:
```vim
  setfacl -m [u|g|o]:RelativeName:(r,w,x) /Path
  setfacl -x [u|g|o]:RelativeName:(r,w,x) /Path
  setfacl -b /Path
```
3. Set resursively:
```vim
  setfacl -R -m ...
```
4. Set or Remove Default:
```vim
  setfacl -d -R -m ...
  setfacl -k ...
```
```bash
  setfacl -d -R -m ...
  setfacl -k ...
```
5. Set from File: the content of file would be like the output of `getfacl`
```bash
  setfacl --restore=acl.txt
```

[top](#file-information)
## Comparison
- [Diff](#diff)
- [Patch](#patch)
- [Cmp](#cmp)
- [Comm](#comm)

[top](#external-bash-command)
### Diff
`diff` output differences of two text files.
1. look at the sample:
```bash
  cat << EOF > file1
  this line hase same words and Line Number in both
  this line hase same words but No2 in first and No3 in Second
  this is a fake line in First File
  this line hase same Line Number No4 in both But Differ Words
  First
  thirth
  Fourth
  Extra1
  Extra2
  EOF
  cat << EOF > file2
  this line hase same words and Line Number in both
  Extra Line in Second File
  this line hase same words but No2 in first and No3 in Second
  this line hase same Line Number No4 in both But Differences Words in eachOther
  First
  Second
  thrith
  Fourth
  EOF
  diff file1 file2
```
[top](#comparison)

the output are:
---
> 1a2 <---------------------------- **a** meant _ADD_, add _line 2_ from **file2** after _line 1_ in **file1**
>
> \> Extra Line in Second File
>
> 3,4c4 <-------------------------- **c** meant _Change_, change _lines 3,4_ of **file1** according _line 3_ of **file2**(delete No3 and change No4)
>
> \< this is a fake line in First File
>
> \< this line hase same Line Number No4 in both But Differ Words
>
> \---
>
> \> this line hase same Line Number No4 in both But Differences Words in eachOther
>
> 6c6,7 <-------------------------- change `thrith` to `Second` and _add_ `thrith` into **file1** according **file2**
>
> \< thirth
>
> \---
>
> \> Second
>
> \> thrith
>
> 8,9d8 <-------------------------- **d** meant _Delete_, delete _lines 8,9_ of **file1**
>
> \< Extra1
>
> \< Extra2
---

[top](#comparison)
2. _-c_ give another output type:
```
*** file1	2018-09-30 15:12:43.311309788 +0330
--- file2	2018-09-30 15:12:56.381125034 +0330
***************
*** 1,9 ****
  this line hase same words and Line Number in both
  this line hase same words but No2 in first and No3 in Second
! this is a fake line in First File
! this line hase same Line Number No4 in both But Differ Words
  First
! thirth
  Fourth
- Extra1
- Extra2
--- 1,8 ----
  this line hase same words and Line Number in both
+ Extra Line in Second File
  this line hase same words but No2 in first and No3 in Second
! this line hase same Line Number No4 in both But Differences Words in eachOther
  First
! Second
! thrith
  Fourth
```

[top](#comparison)
that are:

character|meaning
---|---
!|Indicates that this line is part of a group of one or more lines that needs to change.
+|Indicates a line in the second file that needs to be added to the first file.
-|Indicates a line in the first file that needs to be deleted.

3. _-e_ option useful if you'd like to generate **ed** editor _script_. the outpu for above:
```
  8,9d
  6c
  Second
  thrith
  .
  3,4c
  this line hase same Line Number No4 in both But Differences Words in eachOther
  .
  1a
  Extra Line in Second File
  .
```
and coomand could be : 
```bash
  cat <(diff -e file1 file2) <(echo -e "w\nq") | ed - file1
```
4. _-u_  is useful to create patch file.
5. some other more useful options:

option|description
---|---
-b|Ignore any changes which only change the amount of whitespace (such as spaces or tabs).
-w|Ignore whitespace entirely.
-B|Ignore blank lines when calculating differences.
-y|Display output in two columns.

6. Use **zdiff** to compare _gzipped_ files.
7. Use **diffstat** to create a _histogram (point-distribution graph)_ of output from diff.
8. _diff3_ is An extended version of diff that compares three files at a time.
9. _sdiff_: Compare and/or edit two files in order to merge them into an output file. this command more useful in a script because its an interactive nature.


[top](#comparison)
### Patch
as above when we generate _patch.file_ with `diff -u` we could patch source file with `patch` command which is simplest version updating tools.
1. -_pNo_ option used when file[s] in subdirectories.
2. -_-b_ used for making backup.
* sample: to patch existing **tar.gz** file with newer:
```bash
  diff -Nura file1.tar.gz file2.tar.gz > Diff.patch
  patch < Diff.patch
```
3. _merge_ is an interesting adjunct to diff3.The result is the changes that lead from file1 to file2.**Consider this command a stripped-down version of patch.**
```bash
  merge Mergefile file1 file2
```

[top](#comparison)
### cmp
is simpler `diff` command that only show the first differ occured.
> file1 file2 differ: byte 51, line 2
* Use _zcmp_ on **gzipped** files.

[top](#comparison)
### comm
a versatile files comparison tool. **be careful** files should sorted. the about of above files:
```
Extra1
Extra2
                                                                Extra Line in Second File
                                                                                                                                                    First
                                                                                                                                                    Fourth
                                                                Second
thirth
this is a fake line in First File
                                                                this line hase same Line Number No4 in both But Differences Words in eachOther
this line hase same Line Number No4 in both But Differ Words
                                                                                                                                                    this line hase same words and Line Number in both
                                                                                                                                                    this line hase same words but No2 in first and No3 in Second
                                                                thrith
```
1. _-1_ suppresses column 1
2. _-2_ suppresses column 2
3. _-3_ suppresses column 3
4. _-12_ suppresses both columns 1 and 2, etc.

[top](#comparison)
## Utilities
- [BaseName and DirName](#basename-dirname)
- [Split,cSplit](#splitcsplit)
- [Encoding and Encryption](#encoding-and-encryption)
- [UUencode and UUdecode](#uuencode-and-uudecode)

[top](#external-bash-command)
### BaseName and DirName
from the one path:
  1. _basename_ return back only **file Name** and
  2. _dirname_ return back only **path**.

[top](#utilities)
### Split,cSplit
- _Split_ split file to smaller chunks while
- _cSplit_ split file according mathing pattern

[top](#utilities)
### Encoding and Encryption
- [Sum and ckSum](#sum-and-cksum)
- [md5Sum](#md5sum)
- [sha1Sum](#sha1sum)

[top](#utilities)
#### Sum and ckSum
both check file size but in different size and in **cksum** you could specified different _Algorithm_ .
* **becareful** on _localization_ setting on _source_ and _destination_ machine. 

[top](#ncoding-and-encryption)
#### md5Sum
to check file integrity. 
```bash
  md5sum filename <------------------------ would generate a hash code like e71fbaa0e1af546b4cf975acb36e2cf4 filename
  md5sum filename > checksumfile <--------- put output in file to next using
  md5sum -c checksumfile filename  <------- check integrity with file.
  md5sum -c checksumfile  <---------------- although you could use it alone
```
the output of last command would be
---
> test: FAILED
>
> md5sum: WARNING: 1 computed checksum did NOT match
---

[top](#ncoding-and-encryption)
#### sha1Sum
like aboves in different algorithm.

[top](#ncoding-and-encryption)
### UUencode and UUdecode
these are useful to encode/decode _Binary_ file like **Multimedia**,**Image**,**Compress**,... .
**The uuencode** _utility_ encodes **eight-bit** data into a **seven-bit** representation for sending via _email_ or on Usenet.
therefor if you would send binary file like tar via mail, should code like:
```bash
  tar -cf -  Files | gzip  - | uuencode -  | mail -s "Test" vhp1360@yahoo.com
```
and to return back: first save body mail as file name
```bash
  uudecode filename.tar.gz.uu | gunzip | tar -xf -
```
### Mimencode and mmencode
it is another encoding and **base64** is borth from this.
```bash
  mimencode ...
  mimencode -u ...
```

[top](#utilities)
### Crypt
### OpenSSl
we could use this Package as encryption tools too.
```bash
  openssl aes-128-ecb -salt -in FileName -out FileName.encrypted -pass pass:Password
  openssl aes-128-ecb -d -salt -in FileName.encrypted -out FileName -pass pass:Password
```
### Shread
it is useful to overwrite file or block data to secure it.
```bash
  shread -u -f -n 5 FileName
```
    - _-f_ to change file[s] permission if needed
    - _-n No_ for number rewriting
    - _-x_ prevent to roundup file size to next block
    - _-z_ adds a final overwrite with zeros to hide shredding
    - _-u_ to removing file after trunction

[top](#utilities)
## Miscellaneous
- [mktemp](#mktemp)
- [make](#make)
- [install](#install)
- [Dos2Unix](#dos2unix)
- [PTX](#ptx)
- [More,Less](#moreless)

[top](#external-bash-command)
### Mktemp
it is useful atleat for two reason:
  1. first security, because _permission_ would be *600*.
  2. second prevent of **Collision** when multiple of _Application_ running.
```bash
  mktemp SampleNameXXXX -d --suffix=Vhp -p TempDir
```
### make
### install
### Dos2Unix
### PTX
### More

[top](#utilities)
## Commutication Commands
- [Information and Statistics](#information-and-statistics)
  - [Host](#host)
  - [Ipcalc](#ipcalc)
  - [nslookup](#nsloolup)
  - [Dig](#dig)
  - [Traceroute](#traceroute)
  - [Ping](#ping)
  - [Whois](#whois)
  - [Finger](#finger)
  - [Chfn](#chfn)
  - [vrfy](#vrfy)
- [Remote Host Access](#remote-host-access)
  - [S|r/x|z](#srxz)
  - [Ftp and Sftp](#ftp-and-sftp)
  - [Uucp,uux,cu](#uucpuuxcu)
  - [Telnet](#telnet)
  - [Wget](#wget)
  - [Lynx](#lynx)
  - [RLogin,RSh,RCp](#rloginrshrcp)
  - [RSync](FilesAndPartitioning.md#rsync)
  - [SSH](#ssh)
- [Local Network](#local-network)
  - [Write](#write)
- [Mail Commands](#mail-commands)
  - [mail](#mail)

[top](#external-bash-command)
### Information and Statistics
#### Host
to find **Host Name** _IP Address_:
```bash
  host Fully.Qualified.Domain.Name
```
#### Ipcalc
to find **Host Name** from **IP Address** and vice versa.
```bash
  ipcalc -h IPAddr
  ipcalc -h HostName
  ipcalc --all-unfo IPAddr
```
- the out of last may:
***
> Address:	192.110.96.1
>
> Address space:	Internet
>
> Address class:	Class C
***

[top](#information-and-statistics)
#### nslookup
it is more powerfull of Previouses:
  1. find Host Name or IP Addresses:
  ```bash
    nslookup FQDN
    nslookup IPAddress
  ```
  2. find [DNS Names|Exhange Mail Names| All DNSs]: `nslookup -query=[ns|mx|any] ...`
  3. **SOA** information: `iplookup -type=soa ...`
  4. debugging: `nslookup -debug FQDN`
  5. Query From Specific DNS: `nslookup FADN DNSServer`
  6. Change _Time out_ response: `nslookup -timeout=No ...`

[top](#information-and-statistics)
#### Dig
is **D**omain **I**nformation **G**roper and used to find both IP Addr and Server name and like the [nslookup](#nslookup) but with easier commands.
you also you could create **.digrc** file to preserve _dig_ command _configs_.
* _dig_ use **OS Library** but _nslookup_ used **itself Library** for that **_ISC_** decided to depricate this command in later.

  1. _-x_ used to reverse lookup.
  2. _+time_ to increase **timeout**.
  3. _-nofail_ to prevent failling until get result.
  4. ignore _+nocomments_ , _+noquestion_ , _+noauthority_ , _+noadditional_ , _+nostats_ , _+noall_ , _+answer_ Sections from output.
```bash
  dig FQDN1 [MX|NS|SOA|TTL] +noall ... FQDN2 ...
```
#### Traceroute
* Somtetimes may you have _Ping_ but **traceroute** wonn't response. it occure the _Destination Server_ doesn't answer to **TCP** then try with \
* `traceroute -U ...` or `tracerout -I ...` for **UDP** protocol or **ICMP** protocol
#### Ping

[top](#information-and-statistics)
#### Whois
Perform a DNS (Domain Name System) lookup. The -h option permits specifying which particular whois server to query. 

[top](#information-and-statistics)
#### Finger
Retrieve information about users on a network. Optionally, this command can display a user's ~/.plan, ~/.project, and ~/.forward files, if present.
#### Chfn
Change information disclosed by the finger command.
#### vrfy
verified an Internet Email Address.
### Remote Host Access
- [S|r/x|z](#srxz)
- [Ftp and Sftp](#ftp-and-sftp)
- [Uucp,uux,cu](#uucpuuxcu)
- [Telnet](#telnet)
- [Wget](#wget)
- [Lynx](#lynx)
- [RLogin,RSh,RCp](#rloginrshrcp)
- [RSync](FilesAndPartitioning.md#rsync)
- [SSH](#ssh)

#### S|r/x|z
for **Send** and **Receivieng** _file_ to/from **Xmodem/Zmodem**.
#### Ftp and Sftp
used to connect to remote server for file transferring.
```bash
  ftp [UserName@]IPAddr|HostName
``` 
if connection established, it asks you password.
  - _ls_ to list existing files and folders
  - _get/put_ to download/upload file
  - _mget/mput_ to download/upload file**s**
  - _cd/lcd_ change remote/local Directory
  - _ascii/binary_ set file transferring mode

**Sftp** is _secure_ connection that used **SSH** protocol to connect and transferring.

[top](#remote-host-access)
#### Uucp,uux,cu
**uucp**: UNIX to UNIX copy. This is a communications package for transferring files between UNIX servers. 
A shell script is an effective way to handle a uucp command sequence.
Since the advent of the Internet and e-mail, **uucp** seems to have faded into obscurity, but it still exists and remains perfectly workable in situations 
where an Internet connection is not available or appropriate. 
The advantage of uucp is that it is fault-tolerant, so even if there is a service interruption the copy operation will resume where it left off when 
the connection is restored.

**uux**: UNIX to UNIX execute. Execute a command on a remote system. This command is part of the uucp package.

**cu**: Call Up a remote system and connect as a simple terminal. It is a sort of dumbed-down version of telnet. This command is part of the uucp package.

[top](#remote-host-access)
#### Telnet
[Some fun Telnet](https://www.digitalcitizen.life/5-fun-geeky-things-you-can-do-telnet-client):
1. _War Stars_: `telnet towel.blinkenlights.nl 23`
2. _Check The Weather Forecast_ : `telnet rainmaker.wunderground.com`
3. _Play Chess_ : `telnet freechess.org`
4. _Talk To Eliza The AI Psychotherapist_ : `telnet telehack.com`

[top](#remote-host-access)
#### Wget
it is a best tools to download files from Internet and support _Http_ , _Https_ , _Ftp_ and _Sftp_ protocols.
```bash
  wget URLS <-------------------------------------------------------------------- Simplest way to down
  wget -m URL <----------------------------------------------------------------- Download entire of website
  wget -O /Path2Save/NewName <-------------------------------------------------- Save File in New Name And Path
  wget -o LogFile ... <--------------------------------------------------------- Redirecting Log to File
  wget -q ... <----------------------------------------------------------------- Silent Download
  wget -b ... <----------------------------------------------------------------- Done in Background
  wget -i UrlFile <------------------------------------------------------------- each line in UrlFile is URL to Download
  wget -c ... <----------------------------------------------------------------- resume download from previous state if failed
  wget -t No. ... <------------------------------------------------------------- retry downloading No. times if failed
  wget --no-check-certificate ... <--------------------------------------------- skip SSL/TLS checking
  wget --limit-rate=500k <------------------------------------------------------ Limit Down bandwidth
  wget --no-use-server-timestamps ... <----------------------------------------- to set local machine time for downloaded file
  wget -w No. ... <------------------------------------------------------------- No. second wait to start next downloading
  wget --user=UserName --password=Password|--ask-password URL <----------------- Clear
  wget --[http|ftp]-user=UserName [http|ftp]-password=Password|--ask-password ..
  wget --debug -S
```
#### Lynx
it is a commandline web browser
#### RLogin,RSh,RCp
these are old remote tools so used ssh instead.
#### SSH
[here](AdminSecurityActivity.md#ssh) are some notes regarding _Cofigurations_
Some tips:
1. access remote machine Network with **Socks** Tunnel:
```bash
  ssh -D CustomPortNo -f -C -q -N RemoteUserName@RemoteMachine 
  export http_proxy="http://localhost:CustomPortNo"
  export https_proxy="http://localhost:CustomPortNo"
```
   ***
   * _-D_ for creating **Sucks** Tunnel
   * _-f_ to **forking** current ssh and _keep away while **closing** terminal_
   * _-C_ to Compress Data
   * _-q_ , _-N_ to _quit_ mode and told to **SSH** no any entered _Command_ .
   ***

2. Port Forwarding:
```bash
  ssh -N -LlPort:localhost:rPort UserName@RemoteMachine <------- lPort is Localmachine Port and rPort is Remotemachine Port
  ssh -N -RlPort:localhost:rPort UserName@RemoteMachine
```
  ***
  * _-L_ forward local Port to Remote so you could serve remote port on **localhost:lPort**
  * _-N_ forward Remote Port to Local so you could Serve local port on Remote Machine with **LocalMACHINE:rPort**
  ***

3. Outpot Local Microphone on Remote's Speaker: although the quality is very bad
```bash
  dd if=/dev/dsp | ssh -c arcfour -C UserName@Remote dd of=/dev/dsp
```
4. Remote file Mounting:
```bash
  ssh UserName@Remote:/Path/2/Folder /Local/Path/2/Mounting
```
[top](#information-and-statistics)
[top](#communication-commands)
[top](#)
5. Ssh to unreachable machine through reachable machine:
```bash
  ssh -t ReachableMachine UnReachableMachine
```
6. Capture RemoteMachine Packates:
```bash
  ssh root@Remote ‘tshark -f “port !22” -w -‘ | wireshark -k -i –
  OR
  ssh root@example.com tcpdump -w – ‘port !22’ | wireshark -k -i –
```
7. Command Could run While run **ssh** command

SSH|Command	Explanation
---|---
ls|Show directory contents (list the names of files).
cd|Change Directory.
mkdir|Create a new folder (directory).
touch|Create a new file.
rm|Remove a file.
cat|Show contents of a file.
pwd|Show current directory (full path to where you are right now).
cp|Copy file/folder.
mv|Move file/folder.
grep|Search for a specific phrase in file/lines.
find|Search files and directories.
vi|nano	Text editors.
history|Show last 50 used commands.
clear|Clear the terminal screen.
tar|Create & Unpack compressed archives.
wget|Download files from the internet.
du|Get file size.
8. OTHER SSH COMMANDS:There are other SSH commands besides the client ssh. Each has its own page.

SSH Command|Explanation
ssh-keygen|creates a key pair for public key authentication
ssh-copy-id|configures a public key as authorized on a server
ssh-agent|agent to hold private key for single sign-on
ssh-add|tool to add a key to the agent
scp|file transfer client with RCP-like command interface
sftp|file transfer client with FTP-like command interface
sshd|OpenSSH server

[top](#information-and-statistics)
[top](#communication-commands)
[top](#)
### Local Network
#### Write
this command useful for sending message via Terminal into another openned terminal:
1. first find destination terminal with **who** command, the output may like:
> UserName pts/No1 date...
2. Send message with some Command:
```bash
  echo "Message" > /dev/pts/No1
  write UserName pts/No1
  wall
```
* the **write** and **wall** command bring you to a Interactie environment to write messages

### Mail Commands
#### Mail
with Bash you could send and read mails in some ways:
```bash
  mail -a AttachFile -s "Subject" Reciever1@Maill,Reciever2@Maill <------------------------- it let you write your messages in lines and end with <Ctrl><D>
  mail -a AttachFile -s "Subject" -S replayto=Reciever1@Maill,Reciever2@Maill
  cat BodyMessageasFile | mail -a AttachFile -s "Subject" Reciever1@Maill -c Reciever@Maill -b Reciever@Maill -aFrom:Sender@MailAddr 
  mail -a AttachFile -s "Subject" Reciever1@Maill < BodyMessageasFile
  mail -a AttachFile -s "Subject" Reciever1@Maill <<< "Here Text"
  uuencode AttachFile | mail-s "Subject" Reciever1@Maill -c Reciever@Maill -b Reciever@Maill -aFrom:Sender@MailAddr 
  sendmail UserName@MailAddr < BodyMessageasFile
  mutt -s "Subject" -a AttachFile -- Reciever1@Maill < BodyMessageasFile
  cat BodyMessageasFile | mailx -a AttachFile -s "Subject" Reciever1@Maill -c Reciever@Maill -b Reciever@Maill -aFrom:Sender@MailAddr 
```
and to read mails in terminal only run `mail` command.


[top](#external-bash-command)
[top](#)
[top](#)
[top](#)
[top](#)






