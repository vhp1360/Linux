#!/bin/bash
User=${User:-SomeThing}
ResultrPath=ResultPath
[[ ! -a ResultPath/ ]] && mkdir -p ResultPath
read -p 'نحوه کارکرد برنامه: 1.پارامترهاي Hosts(h),Nagios(n),Httpd(ht),Partitions(p),Motd(mt),Memory(m),Cpu(c),Volume(v),IP(i),Mac(mc),Crontab(cr),LBConfig(lb),JNDINames(j),All مي باشند.2.برنامه فايل IPs را مي خواند'
[[ $# != 1 ]] && read -p 'نحوه کارکرد برنامه: 1.پارامترهاي Hosts(h),Nagios(n),Httpd(ht),Partitions(p),Motd(mt),Memory(m),Cpu(c),Volume(v),IP(i),Mac(mc),Crontab(cr),LBConfig(lb),JNDINames(j),All مي باشند.2.برنامه فايل IPs را مي خواند' && exit
for q in $(grep -v '#' $(dirname $0)/IPs)
do
	echo $q
	sed -i '/\[localhost\]\:8008/d' /home/hvatani/.ssh/known_hosts
	ssh -oConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no -fT -L 8008:localhost:22 $User@$q 'while [[ ! -f /tmp/vhp.tmp ]];do sleep 10;done;rm -f /tmp/vhp.tmp'
	RemoteHost=$(ssh -p 8008 localhost "hostname")
	case $1 in
		All)
			ssh -p 8008 localhost "df -h | sort " > ResultPath/$q.df
			ssh -p 8008 localhost "sudo /bin/bash -c 'fdisk -l' | grep 'dev/sd'"> ResultPath/$q.fdisk
			ssh -p 8008 localhost "cat /etc/hosts" > ResultPath/$q.hosts
			ssh -p 8008 localhost "cat /usr/local/nagios/etc/nrpe.cfg" > ResultPath/$q.nagios
			[[ $RemoteHost == *'IHS'* ]] && ssh -p 8008 localhost "sudo /bin/bash -c 'cat /opt/IBM/HTTPServer/conf/httpd.conf'" > ResultPath/$q.httpd
			ssh -p 8008 localhost "cat /etc/motd" > ResultPath/$q.motd
			ssh -p 8008 localhost "sudo /bin/bash -c 'crontab -l'" > ResultPath/$q.crontab
			[[ $RemoteHost == *'LB'* ]] && ssh -p 8008 localhost "sudo /bin/bash -c 'cat /opt/IBM/WebSphere/Edge/ULB/servers/configurations/dispatcher/*LB*.cfg'" > ResultPath/$q.LBConf
			[[ $RemoteHost == *'DMGR'* ]] && scp -P 8008 $(dirname $0)/JNDI_Details.bash localhost:~/ && ssh -p 8008 localhost "sudo /bin/bash ~/JNDI_Details.bash" > ResultPath/$q.JNDI
			;;
		h)
			ssh -p 8008 localhost "cat /etc/hosts" > ResultPath/$q.hosts
			;;
		n)
			ssh -p 8008 localhost "cat /usr/local/nagios/etc/nrpe.cfg" > ResultPath/$q.nagios
			;;
		ht)
			[[ $RemoteHost == *'IHS'* ]] && ssh -p 8008 localhost "sudo /bin/bash -c 'cat /opt/IBM/HTTPServer/conf/httpd.conf'" > ResultPath/$q.httpd
			;;
		v)
			ssh -p 8008 localhost "sudo /bin/bash -c 'fdisk -l' | grep 'dev/sd'"> ResultPath/$q.fdisk
			;;
		p)
			ssh -p 8008 localhost "df -h | sort" > ResultPath/$q.df
			;;
		mt | m | mc | c | i)
			ssh -p 8008 localhost "cat /etc/motd" > ResultPath/$q.motd
			;;
		cr)
			ssh -p 8008 localhost "sudo /bin/bash -c 'crontab -l'" > ResultPath/$q.crontab
			;;
		j) 
			[[ $RemoteHost == *'DMGR'* ]] && scp $(dirname $0)/JNDI_Details.bash -P 8008 localhost:~/ && ssh -p 8008 localhost "sudo /bin/bash ~/JNDI_Details.bash" > ResultPath/$q.JNDI
			;;
		lb) 
			[[ $RemoteHost == *'LB'* ]] && ssh -p 8008 localhost "sudo /bin/bash -c 'cat /opt/IBM/WebSphere/Edge/ULB/servers/configurations/dispatcher/*LB*.cfg'" > ResultPath/$q.LBConf
			;;
	esac
	ssh -p 8008 localhost touch /tmp/vhp.tmp
	kill -9 $(ps faux | grep ssh | grep 8008 | awk '{print $2}')
done
echo 'خروجي برنامه در مسير ~/ResultPath و بانام IP.8 مي باشد'
