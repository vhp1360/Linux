#!/bin/bash
echo '==============================================توجه=============================================================='
echo '===================== خروجي برنامه در مسير ~/DataSourceDetails قرار داده مي شود. ====================='
echo '================================================پ==============================================================='
ResultPath=~/DataSourceDetails
mkdir -p $ResultPath
IpType=${IpType:-1}
UserName=$USER
UserPass=${UserPass:-SomeThings}
Root1=${Root1:-SomeThings}
Root2=${Root2:-SomeThings}

WasAddr='/opt/IBM/WebSphere/AppServer/profiles/*/bin/wsadmin.sh'
DmgrAddr='/opt/IBM/WebSphere/AppServer/profiles/*/DPManager/'
SecFile='/opt/IBM/WebSphere/AppServer/profiles/*/config/cells/*/security.xml'
SinglersrcFile='/opt/IBM/WebSphere/AppServer/profiles/*/config/cells/*/nodes/*/servers/*/resources.xml'
DmgrrsrcFile='/opt/IBM/WebSphere/AppServer/profiles/*/config/cells/*/clusters/*/resources.xml'
if [[ -a /opt/IBM/WebSphere/AppServer/java/bin/java ]]
then
	passEnPath='/opt/IBM/WebSphere/AppServer/java/bin/java -Djava.ext.dirs=/opt/IBM/WebSphere/AppServer/plugins:/opt/IBM/WebSphere/AppServer/lib com.ibm.ws.security.util.PasswordEncoder' 
	passDecPath='/opt/IBM/WebSphere/AppServer/java/bin/java -Djava.ext.dirs=/opt/IBM/WebSphere/AppServer/plugins:/opt/IBM/WebSphere/AppServer/lib com.ibm.ws.security.util.PasswordDecoder'
else
	passEnPath='/opt/IBM/WebSphere/AppServer/java/*/bin/java -Djava.ext.dirs=/opt/IBM/WebSphere/AppServer/plugins:/opt/IBM/WebSphere/AppServer/lib com.ibm.ws.security.util.PasswordEncoder'
	passDecPath='/opt/IBM/WebSphere/AppServer/java/*/bin/java -Djava.ext.dirs=/opt/IBM/WebSphere/AppServer/plugins:/opt/IBM/WebSphere/AppServer/lib com.ibm.ws.security.util.PasswordDecoder'
fi
ServerTypePath='/opt/IBM/WebSphere/AppServer/bin/serverStatus.sh -all'

#HostName,uID,alias,userId,password
#***********************************************************Functions********************************************
#sed -i '/\[localhost\]\:8008/d' /home/hvatani/.ssh/known_hosts
#ssh -oConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no -fT -L 8008:localhost:22 $User@$q 'while [[    ! -f /tmp/vhp.tmp ]];do sleep 10;done;rm -f /tmp/vhp.tmp' 
#RemoteHost=$(ssh -p 8008 localhost "hostname")

#***********************************************************Functions********************************************
function is_was() {
	Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'ls $WasAddr > /dev/null && echo \"True\" || echo \"False\"'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "ls $WasAddr > /dev/null && echo 'True' || echo 'False'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "ls $WasAddr > /dev/null && echo 'True' || echo 'False'")
	echo $Res
	}
function server_Type() {
	Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'timeout 40 /opt/IBM/WebSphere/AppServer/bin/serverStatus.sh -all | grep \"Server name\"'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "timeout 40 /opt/IBM/WebSphere/AppServer/bin/serverStatus.sh -all | grep 'Server name'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "timeout 40 /opt/IBM/WebSphere/AppServer/bin/serverStatus.sh -all | grep 'Server name'")
	if [[ $Res == *'nodeagent'* ]];then echo 'Node'
	elif [[ $Res == *'dmgr'* ]];then echo 'Dmgr'
	elif [[ $Res == *'server1'* && $Res == *'webserver1'* ]];then echo 'StandAlone'
	fi
}
function hosts_Info() {
	Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'grep \"WAS\\|IHS\\|LB\" /etc/hosts | sed \"s/\\./\\ /4;s/\\s\\s*/,/g\" | cut -d, -f1,2 | sort -t, -k2 | tr \"\\n\" \"#\"'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep 'WAS\|IHS\|LB' /etc/hosts | sed 's/\./\ /4;s/\s\s*/,/g' | cut -d, -f1,2 | sort -t, -k2 | tr '\n' '#'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep 'WAS\|IHS\|LB' /etc/hosts | sed 's/\./\ /4;s/\s\s*/,/g' | cut -d, -f1,2 | sort -t, -k2 | tr '\n' '#'")
	echo $Res
	echo $Res
}

function info_dbUser () {
	Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c \"grep authDataEntries $SecFile\"")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep 'authDataEntries' $SecFile")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep 'authDataEntries' $SecFile")
	echo $Res
}
function info_dbSource () {
        # variables $1=Alias
	Res=
	if [[ $SrvTyp == 'Dmgr' ]] 
	then 
		Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'grep \"$2\" $rsrcFile | grep \"resources.jdbc:DataSource\"'")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep \"$2\" $rsrcFile | grep 'resources.jdbc:DataSource'")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep \"$2\" $rsrcFile | grep 'resources.jdbc:DataSource'")
	elif [[ $SrvTyp == 'StandAlone' ]]
	then 
		Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'grep \"$2\" $rsrcFile | grep \"resources.jdbc:DataSource\"'")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep $2 $rsrcFile | grep 'resources.jdbc:DataSource'")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep $2 $rsrcFile | grep 'resources.jdbc:DataSource'")
	elif [[ $SrvTyp == 'Node' ]]
	then
		echo
	fi
		echo $Res
}
function db_info() {
	# variables $1=Main Id 
	Res=
	if [[ $SrvTyp == 'Dmgr' ]]
	then       
		Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'grep \"'$2'\" '$DmgrrsrcFile' | grep '$3")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep \"$2\" $DmgrrsrcFile | grep $3")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep \"$2\" $DmgrrsrcFile | grep $3")
	elif [[ $SrvTyp == 'StandAlone' ]]
	then
		Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'grep \"'$2'\" $SinglersrcFile | grep '$3")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep \"$2\" $SinglersrcFile | grep $3")
		[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "grep \"$2\" $SinglersrcFile | grep $3")
        elif [[ $SrvTyp == 'Node' ]]
	then
                echo
        fi
	echo $Res
}
function changePass() {
#	passEnPath="/opt/IBM/WebSphere/AppServer/java/bin/java -Djava.ext.dirs=/opt/IBM/WebSphere/AppServer/plugins:/opt/IBM/WebSphere/AppServer/lib com.ibm.ws.security.util.PasswordEncoder"
#	passDecPath="/opt/IBM/WebSphere/AppServer/java/bin/java -Djava.ext.dirs=/opt/IBM/WebSphere/AppServer/plugins:/opt/IBM/WebSphere/AppServer/lib com.ibm.ws.security.util.PasswordDecoder"
	# $1={1=> encode,2=> decode} $2=InputVal
	Q=\'
	case $2 in
		0) 
			Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c $Q$passEnPath $3$Q")
			[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "$passEnPath $3")
			[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "$passEnPath $3")
		;;
		1) 
			Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c $Q$passDecPath $3$Q")
			[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "$passDecPath $3")
			[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "$passDecPath $3")
		;;
	esac
	echo $Res
}
function exec_Change () {
	Res=$(SSHPASS=$UserPass sshpass -e ssh -o ConnectTimeout=10 -t -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no $UserName@$1 "sudo /bin/bash -c 'sed -i.$(date \"+%Y%m%d_%H%M%S\") \"s/'$1'/'$2'/\" '$SecFile' && Res+=\"OK\" || Res=\"NotOk\"'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root1 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "sed -i.$(date '+%Y%m%d_%H%M%S') \"s/$1/$2/\" $SecFile && Res+='OK' || Res='NotOk'")
	[[ -z $Res ]] && Res=$(SSHPASS=$Root2 sshpass -e ssh -o ConnectTimeout=10 -c aes128-ctr,aes192-ctr,aes256-ctr -oStrictHostKeyChecking=no root@$1 "sed -i.$(date '+%Y%m%d_%H%M%S') \"s/$1/$2/\" $SecFile && Res+='OK' || Res='NotOk'")
	echo $Res
}
#**************************************************************************************************************************************************************************
function JNDIsInfo() {
	[[ $(is_was $1 ) == 'False' ]] && echo -e '{\n"wWasExist" : "False"\n}' && continue
	Text1="{\"IP Addr\": \"$1\",\n"
	Text2='IPAddr,WasExist,SrvTyp,UserId,Alias,UserName,Password,MainId,datasourceName,jndiName,DBName,DBIPAddr,decode\n'
	Text1+='\n\t"WasExist": "True",\n'
	Text2hlp="$1,True,"
	SrvTyp=$(server_Type $1)
	if [[ $SrvTyp == *'Node'* ]]
	then 
		rsrcFile=;
		Text1+='\t"SrvTyp": "Node",\n'
		Text2hlp+='Node,'
	elif [[ $SrvTyp == *'StandAlone'* ]]
	then 
		rsrcFile=$SinglersrcFile;
		Text1+='\t"SrvTyp": "StandAlone",\n'
		Text2hlp+='StandAlone,'
	elif [[ $SrvTyp == 'Dmgr' ]]
	then 
		rsrcFile=$DmgrrsrcFile;
		Text1+='\t"SrvTyp": "Dmgr",\n'
		Text2hlp+='Dmgr,'
		nSrvRes=$(echo -e $(hosts_Info $1) | tr '#' '\n')
		Text1+="\t\"SrvName\": ["
		for line in $nSrvRes
		do
			Text1+="\n\t\t{\n\t\t\t\"ServerName\": \"$(echo $line | cut -d, -f2)\",\n\t\t\t\"IPAddr\": \"$(echo $line | cut -d, -f1)\"\n\t\t},"
		done
		Text=${Text%?}
		Text1+="\n\t],"
	fi
	Res=$(info_dbUser $1)
	nRes=$(echo -e $Res | tr '>' '\n' | tr ' ' '#'| grep -v "^\s*$" )
	if [[ ! -z $nRes ]]
	then
		Text1+="\n\t\"UserNames\": ["
		for line in $nRes
		do
			uID=$(echo -n $line | cut -d\" -f2 | cut -d_ -f2);alias=$(echo -n $line | cut -d\" -f4);userId=$(echo -n $line | cut -d\" -f6);password=$(echo -n $line | cut -d\" -f8)
			dbsrcRes=$(info_dbSource $1 $alias)
			tID=$(echo -n $dbsrcRes | cut -d\" -f4 | cut -d_ -f2);mID="${tID:0:$(expr $(expr length $uID)-2)}"
			dbsrcName=$(echo $dbsrcRes | cut -d\" -f6);dbJndiName=$(echo $dbsrcRes | cut -d\" -f6)
			dbRes=$(db_info $1 $mID "databaseName");dbName=$(echo $dbRes | cut -d\" -f8)
			dbipRes=$(db_info $1 $mID "serverName");dbIP=$(echo $dbipRes | cut -d\" -f8)
			tpassRes=$(changePass $1 1 $password);decoded=$(echo $tpassRes | cut -d\" -f4)
			Text1+="\n\t\t{\n\t\t\t\"UserId\": \"$uID\",\n\t\t\t\"Alias\": \"$alias\",\n\t\t\t\"UserName\": \"$userId\",\n\t\t\t\"Password\": \"$password\",\n\t\t\t\"MainId\": \"$mID\",\n\t\t\t\"datasourceName\": \"$dbsrcName\",\n\t\t\t\"jndiName\": \"$dbJndiName\",\n\t\t\t\"DBName\": \"$dbName\",\n\t\t\t\"DBIPAddr\":\"$dbIP\",\n\t\t\t\"decode\":\"$decoded\""
			Text2+="$Text2hlp$uID,$alias,$userId,$password,$mID,$dbsrcName,$dbJndiName,$dbName,$dbIP,$decoded\n"
			Text1+="},"
	        done
		Text=${Text%?}
	        Text1+="\n\t]"
        	Text1+="\n}"
	else
		Text=${Text%?}
		Text1+='\t\tUsersExist: "False"\n}'
	fi
	echo "$Text1#$Text2"
#	echo -e $Text >> $ResultPath.$1
}
function fForJob(){
	Text=$(JNDIsInfo $1)
	TextJson=$(echo $Text | cut -d'#' -f1)
	Textcsv=$(echo $Text | cut -d'#' -f2)
	tTextJ+=$TextJson\n
	tTextC+=$Textcsv\n
	echo -e $TextJson > $ResultPath/$1.JNDI.Json
	echo -e $Textcsv > $ResultPath/$1.JNDI.csv
}
tTextJ=
tTextC=
for IP in $(cat $(dirname $0)/WasIpLists)
do
	$(fForJob $IP) &
done
