<div dir="rtl" align="right" style="color:blue"><h1>بنام خدا</h1></div>

```bash
awk 'BEGIN {first assumes;}
            { commands}
     END{OutPuts}'
```
- there are some variables:
     - \-F~FS: input _Field_ Seperator
     - OFS: output _Field_ Seperator
     - RS/ORS: input/output _row_ Seperator
     - NF/R : number of _Field_/_row_
     - $No. : Column Number. _$0_ means all _record_
     - FILENAMW,FIELDWITH,ARGC,ARGV,ENVIRON,FNR
- some Functions:
     - **IF**: `if(Expr) Statement else Statement`
     - **oneLineIF**: `(Condition)?True:False`
     - **FOR**: `for(par=Val;Condition;Expr) Statement` , `for(var in Array) ...`
     - **WHILE**: `while(Condition) Statement`
     - _break,continue,next,exit_
- Mathematic Functions:
     - _sin,cos,atan,log,sqrt,rand,srand,exp_
     - _\+,\-,\*,\/,\%,\+\=,\-\=,..._
- Some String Functions:
     - _index_(String,Search)
     - _length_(Str)
     - _split_(Str,Arr,Seperator)
     - _substr_(Str,Pos[,Max])
     - _sub_(Reg,Rep[,Str])
     - _match_(Str,Reg)
     - _asort_,_asorti_,_to_,_strtonum_
     - **_n=getline_** : return line after changes
- Examples:
     - do in ranges:
          1. `(NR==10),(NR==19){print...}`
          2. `(NR==10),/MyName/ {print...}`
          3. `(NR==8){print...};(NR==10),(NR==18){print...};`
          4. `/MyPatern/ {print...}`
          5. `awk '/First/,/Second/' File`
