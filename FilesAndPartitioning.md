<div dir="rtl">بنام خدا</div>

- [Network File Sharing](#network-file-sharing)
  - [GlusterFS](#glusterfs)
  - [Samba](#samba)
  - [VSFTP](#vsftp)
- [Files System](#files-system)
  - [ISO](#iso)
- [File Properties](#file-properties)
  - [File Owner And Permissions](#file-owner-and-permissions)
  - [File Access List](BashCommnds.md#file-access-list)
  - [File Attributes](#file-attributes)
- [Partitioning](#partitioning)
  - [Parted](#parted)
  - [dd](#dd)
- [Files Operating](#files-operating)
  - [rsync](#rsync)
  - [find](BashCommands.md#find)
- [Encrypt and Decrypt Files](#encrypt-and-decrypt-files)
  - [Gpg](#gpg)

[Top](#top)
# Network File Sharing
### Glusterfs
1. Installation : glusterfs-server gluster-client
2. Make a Partition:
    1. if you have new partition, nothing to do.
	2. if you haven't free partition: pass below steps to create a new partition of existing space!
		1. `touch GlusterFSfile`
		2. `dd if=/dev/zero of=GlusterFSfile bs=8192 count=10000000 --> to create 81G partition`
		3. `mkfs.xfs GlusterFSfile`
		4. `mount -t xfs GlusterFSfile /wherever`
	3. service glusterd start on all machine
	4. `gluster peer probe Other Machines` --> run all machines.
	5. `gluster peer status or list` --> to check .
	6. `gluster pool list` --> also for another layer check.
	7. `gluster volume create GlusterFSDriveName replica No. transport tcp,rdma ThisMachine:/path to GlusterFsfileMountPoint NextMachine:\
	/path to GlusterFsfileMountPoint ...` --> only on one machine.
	8. `gluster volume start GlusterFSDriveName`
	9. finally you need mount new GlusterFSPartition on each machine or client and check permissions.\
	`mount -t glusterfs MachineName:/GlusterFSDriveName /Path to Mount Point`
	10. if you need remove GlusterFSDriveName --> `gluster volume stop GlusterFSDriveName && gluster volume delete GlusterFSDriveName`
	11. if you need remove machine --> `gluster peer detach MachineName`

***
>	1. be care on hosts file: you need complete it and fill loopback IP line with itself machine name too.
>	2. be care on ssh Password Less config
>	3. be care on IPTables
>	4. be care on SeLinux
	
***
[Top](#top)
### Samba
- Securing from sambaCry attack.
   - Which IP:
   ```bash
     hosts deny = ALL
     hosts allow = 192.168.1.
   ```
   - Which version
   ```bash
     server min protocol = SMB2_10
     client max protocol = SMB3
     client min protocol = SMB2_10
   ```
- [Samba And SeLinux](http://danwalsh.livejournal.com/14195.html):
```bash
  setsebool -P samba_domain_controller on
  semanage fcontext -a -t samba_share_t '/<shared path>(/.*)?'
  restorecon -R /<shared path>
```
- [Samba And Iptables](https://wiki.centos.org/HowTos/SetUpSamba#head-26340a1a2c9cb4d46d51b9429fd030239b57feb4):
```bash
-A INPUT -s 192.110.133.0/24 -p tcp -m state --state NEW -m tcp -m multiport --dports 139,445 -j ACCEPT
-A INPUT -s 192.110.133.0/24 -p udp -m state --state NEW -m udp -m multiport --dports 137:138,445 -j ACCEPT
```
- Sharing Folder: to keep more security, we should set chmod ---- and also setfacl, the Kvm user is `qemu`.\
	if we revoke other user acess, we should have same username userid gourpname and groupid.

- mounting:
   - by command:
   ```bash
     mount -t cifs //servername/sharename  /media/windowsshare  username=UserName
   ```
   - put it in fstab:
   ```bash
     //servername/sharename  /media/windowsshare  cifs  username=UserName,password=Password,iocharset=utf8,sec=ntlm  0  0
   ```
   
[Top](#top)
### VSFTP

1. First:
```bash
	yum install vsftpd
	cp /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf.default
```
2. two type of installation:
	
	1. Without Anonymouse User:
		- bash /etc/vsftpd/vsftpd.conf
		```vala
			anonymous_enable=NO # disable  anonymous login
			local_enable=YES	# permit local logins
			write_enable=YES	# enable FTP commands which change the filesystem
			local_umask=022		# value of umask for file creation for local users
			dirmessage_enable=YES	# enable showing of messages when users first enter a new directory
			xferlog_enable=YES	# a log file will be maintained detailing uploads and downloads
			connect_from_port_20=YES # use port 20 (ftp-data) on the server machine for PORT style connections
			xferlog_std_format=YES # keep standard log file format
			listen=YES # only one of listen or listen_ipv6 sould uncommented ,this for running in standalone mode
			listen_ipv6=YES	# vsftpd will listen on an IPv6 socket instead of an IPv4 one
			pam_service_name=vsftpd # name of the PAM service vsftpd will use
			userlist_enable=YES # enable vsftpd to load a list of usernames
			tcp_wrappers=YES # turn on tcp wrappers
			chroot_local_user=YES # for OS user this only loging users to their home and they have regulare access like SSH
			allow_writeable_chroot=YES		
			secure_chroot_dir=/var/run/vsftpd
			######################################
			ssl_enable=YES	# more secure and we use this, when try connect from terminal we should use 'sftp' command
			ssl_tlsv1_2=YES
			ssl_sslv2=NO
			ssl_sslv3=NO
			allow_anon_ssl=NO
			force_local_data_ssl=YES
			force_local_logins_ssl=YES
			require_ssl_reuse=NO
			ssl_ciphers=HIGH
			debug_ssl=YES
			rsa_cert_file=/Path/vsftpd.pem
			rsa_private_key_file=/Path/vsftpd.pem
			#######################################
			pasv_min_port=40000 # to active passive mode
			pasv_max_port=50000
		```
		* if you'd like restrict all user from _ftp_ you:
			1. in Redhat Base: in /etc/sshd_config
			```vala
				Subsystem sftpGroup internal-sftp
				Match Group sftpgroup
				ChrootDirectory /home
				ForceCommand internal-sftp
				X11Forwarding no
				AllowTcpForwarding no				
			```			
			2. in Debian Base: in /etc/vsftpd/vsftpd.conf
			```vala
				userlist_enable=YES # vsftpd will load a list of usernames, from the filename given by userlist_file
				userlist_file=/etc/vsftpd.userlist # stores usernames.
				userlist_deny=NO	
			```
		- SELinux:
		```bash
			setsebool -P ftp_home_dir on								# in redhat before 7
			setsebool -P tftp_home_dir on								# in redhat 7
			semanage boolean -m ftpd_full_access --on
		```
		- IPTables:
		```vala
			-A INPUT -p tcp -m tcp -m state --state NEW -m nultiport --dports 20,21 -j ACCEPT
		```
	
	2. With different user home directory:
		- bash /etc/vsftpd/vsftpd.conf
		```vala
			allow_writeable_chroot=NO
			user_sub_token=$USER         			# inserts the username in the local root directory 
			local\_root=/PathToNewHome/$USER   # defines any users local root directory
		```
		- Create Related Homes and assignes Permission
		```vala
			mkdir /PatjNewHome/USerName
			chown UserName:UserName /PatjNewHome/USerName
			chmod 0700 /PatjNewHome/USerName
		```
	3. Bind Another File to User Home:
	```vala
		mkdir /home/UserName/BundlePath
		mount --bind /Path/BundlePath /home/UserName/BundlePath
	```
	
	
[Top](#top)
# Files System
### ISO

[Top](#top)
# File Properties
### File Owner And Permissions](#file-owner-and-permissions)
### File Attributes
attributes are properties of a file that regulate how the operating system interacts with a given file. 
There are 15 file attributes: 

  - append only (a), 
  - no atime updates (A), 
  - compressed (c), no copy on write (C), 
  - no dump (d), 
  - synchronous directory updates (D), 
  - extent format (e), 
  - immutable (i), 
  - data journalling (j), 
  - project hierarchy (P), 
  - secure deletion (s), 
  - synchronous updates (S), 
  - no tail-merging (t), 
  - top of directory hierarchy (T), 
  - undeletable (u). 
 
Even though there are 15 attributes it’s been my experience that (i) immutable, and (a) append only attributes seem to be the only ones that work consistently 
 across filesystems. If you are using an ext2, ext3, or ext4 filesystem then c,s, and u will not be honored by your filesystems. 
 The secure delete (s), and undeletable (u) attributes would definitely be a nice-to-have feature in the future. 
 If you are using btrfs (likely on a SUSE or OpenSUSE based system) then (c) compression will be respected along with a few others (feel free to experiment).

* for example:
```bash
  lsattr
  chattr +i FileName
  chattr -i FileName
```
[Top](#top)

# Partitioning
- [Parted](#parted)
- [dd](#dd)

### Parted
- add partition to existing free space(may raid) :
  1. `# parted` --> so you intered in (parted) mode.
  2. `select /dev/md100` --> inside of disk.
  3. `unit s print free` --> give fisrt and last sector number for free unpatitioned space on disk.
  4. `mkpart`  --> and go ahead and answer questions.
  5. `quit` --> goback to __bash__ mode.
  6. `mkfs. ... NewPartitionName`
[top](#top)
### dd
- create bootable usb:
```bash
  dd if=/Path/to/Image.iso of=/dev/sdx oflag=direct  bs=1048576 && sync
```
### check if usb is bootable
```bash
qemu -hda /dev/sdb or qemu-system-x86_64 -hda /dev/sdb
```
```
  umount /dev/sdc
  mkdosfs -I /dev/sdc -F 32
  isohybrid /path/to/ISO.iso
  dd if=/path/to/ISO.iso of=/dev/sdc bs=4; sync; eject /dev/sdc
```

[top](#top)
# Files Operating
- [rsync](#rsync)
- [find](BashCommands.md#find)

### rsync:
```bash
  rsync -a(~r(recusrsive)l(link)p(permission)o(owner)t(time modify)g(group)D(device,specials) -z(compress) \
  -h(human) -v(verbose) --include --exclude --remove-source-files --delete -u(updated) -d(sync directory) \
  --append(append to shorter files) -L(change symbol link to real) -W(copy file whole) --inplace(directly write) \
  --max-size --bwlimit
```
1. Only Drive your Code:
```bash
  rsync --drive-run ...
```
2. Copy/Sync Files and Directory Locally:
```bash
  rsync -zhv SourceFILE Destination/
  rsync -azhv SourceDIR Destination/
  rsync -azhv -e 'ssh' SourceDir root@IP:/Path
  rsync -azhv [ssh] root@IP:/SourceDir Dest/
```

   2/1. **-a**: preserve permissions,attributes,symblinks, and recursion mode. ~ -rlptgoD this option need *Root* Permission.

   2/2. **-z**: compress files during transffering.

   2/3. **[ssh]**: by this you have secure copy.

   2/4. **r** ~ --recursive

   2/5. **l**: copy symlinks as symlinks

   2/6. **p**: preserve permissions

   2/7. **t**: preserve modification times

   2/8. **g** ~ --group

   2/9. **o** ~ --owner

   2/10. **D**: same as --devices --specials

   2/11. **--devices**: preserve device files (super-user only)

   2/12. **--specials**: preserve special files


[top](#)

3. Progrss bar:
```bash
  rsync -azhv --progress SourceDir Destination/
```
4. Include & Exclude:
```bash
  rsync -azhv --include '*.R' --exclude '*e*.R' SourceDir/ Destination/
```
5. delete source files or destination files
```bash
  rsync -azhv --remove-source-files Source/ Destination/
  rsync -azhv --delete Source/ Destination/
```
     1. --delete: if folder exist in dest but not remain in source will be delete from Dest.
     2.you could use `--remove-source-files` instead of `mv` command.

6. files size & Bandwidth limitation: will sync less or equeal than
```bash
  rsync -azhv --max-size='20M' --bwlimit=100 ssh ...Source/ Destination/
```
7. resumable:
```bash
  rsync --append ...
```
     1. depending your version may `--append-verify` command, that the same but with _checksum_ implementing.
     2. `--append-no-verify` is in contract above.
8. commit partial copied:
```bash
  rsync --partial[-dir] ...
```
     1. `--partial-dir` used for directory.
     2. `-P` equeal to `--partial --progress`
9. write directly: instead of default _rsync_ behavier, write updates directly to dest:
```bash
  rsync --inplace ...
```
[top](#)
10. sync only Directories tree:
```bash
  rsync -d ...
```
11. sync only existing files in destination:
```bash
  rsync --existing ...
```
12. find different between source and destination:
```bash
  rsync -azvh -i ...
```
13. Transfer the Whole File : it spped up transffering.
```bash
  rsync -azvh -W ...
```
[Top](#)
# Encrypt and Decrypt Files
#### Gpg: 
```vim
  gpg -c FileName -> Create Encrypted file with Passphrase
  gpg FileName.gpg -> Output of File in StdOut
  gpg -d FileName.gpg -o FileName -> SaveFile in Hard
```


[Top](#)
