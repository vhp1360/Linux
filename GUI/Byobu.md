<div dir="rtl" align="right">بنام خدا</div>

- [What is this](##what-is-this)
- [Short Keys](#short-keys)
    - [Sessions](#sessions)
        - [Seassion Creation](#session-creation)
        - [Seassion Detaching](#session-detaching)
        - [Seassion Reanem](#session-rename)
        - [Seassion Moveing](#session-moving)
    - [Windows and Splits](#windows-and-splits)
        - [Creation](#creation)
        - [Movement](#movement)
        - [Resizing,Arrengment,Save and Restoring](#resizingarrengmentsave-and-restoring)
        - [Zoom,Expand and Joint Split](#zommexpand-and-joint-split)
    - [Send Command](#send-command)
    
[top](#top)

# What is this
as my consideration,[Byobu](http://byobu.co/) is most power full multiplexer terminal for keep _ssh session_ on remote server. 

[top](#top)
# Short Keys
## Sessions
#### Seassion Creation
##### <kbd>Shift</kbd><kbd>Ctrl</kbd><kbd>F2</kbd>: Create New Session

#### Seassion Detaching
##### <kbd>F6</kbd>: Completly Detach and Logout
##### <kbd>Shift</kbd><kbd>F6</kbd> Detach From Session but Does not logout

#### Seassion Reanem
##### <kbd>F8</kbd>: Rename current Window
##### <kbd>Ctrl</kbd><kbd>F8</kbd>: Rename Current Session

#### Seassion Moveing
##### <kbd>Alt</kbd><kbd>:arrow_up:</kbd>/<kbd>:arrow_down:</kbd>: Move Between Sessions

[top](#top)
## Windows and Splits
#### Creation
##### <kbd>F2</kbd>: Create New Window
##### <kbd>Shift</kbd><kbd>F2</kbd>: Create New _Horizontal_ **Split**
##### <kbd>Ctrl</kbd><kbd>F2</kbd>: Create New _Vertical_ **Split**
#### Movement
##### <kbd>F3</kbd>/<kbd>F4</kbd>: Backward/Forward moves between Windows
##### <kbd>Shift</kbd><kbd>F3</kbd>/<kbd>F4</kbd>: Backward/Forwaerd moves between Splits
##### <kbd>Ctrl</kbd><kbd>F3</kbd>/<kbd>F4</kbd>: same above
##### <kbd>Shift</kbd><kbd>:arrow_right:</kbd>/<kbd>:arrow_left:</kbd>/<kbd>:arrow_up:</kbd>/<kbd>:arrow_down:</kbd>: Move between Splits
#### Resizing,Arrengment,Save and Restoring
##### <kbd>Alt</kbd><kbd>Shift</kbd><kbd>:arrow_right:</kbd>/<kbd>:arrow_left:</kbd>/<kbd>:arrow_up:</kbd>/<kbd>:arrow_down:</kbd>: Resize Focused Split
##### <kbd>Shift</kbd><kbd>F8</kbd>: Rearragment Splits(Toggle theme)
##### <kbd>Ctrl</kbd>/<kbd>Alt</kbd><kbd>Shift</kbd><kbd>F8</kbd>: Save/Restore current split possions
#### Zoom,Expand and Joint Split
##### <kbd>Shift</kbd><kbd>F11</kbd>: Zoom In/Out focused Split
##### <kbd>Alt</kbd><kbd>F11</kbd>: Expand focused Split
##### <kbd>Ctrl</kbd><kbd>F11</kbd> Joint Current Window as Split to other Windows


[top](#top)
## Send Command
##### <kbd>Ctrl</kbd><kbd>F9</kbd>: run to all current window
##### <kbd>Shift</kbd><kbd>F9</kbd>: run to all current Split.
[top](#top)
