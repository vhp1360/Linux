<div dir="rtl" align="right">بنام خدا</div>
- [Browsh](#browsh)
- [links](#links)
- [lynks](#lynks)


# Browsh

### Install Browsh
- on RHEL/CentOS & Fedora

1- 64-bit:
```bash
  wget https://github.com/browsh-org/browsh/releases/download/v1.4.12/browsh_1.4.12_linux_amd64.rpm
  rpm -Uvh browsh_1.4.12_linux_amd64.rpm
```
2-32-bit:
```bash
  wget https://github.com/browsh-org/browsh/releases/download/v1.4.12/browsh_1.4.12_linux_386.rpm
  rpm -Uvh browsh_1.4.12_linux_386.rpm
```
- on Debian/Ubuntu & Linux Mint

1- 64-bit:
```bash
  wget https://github.com/browsh-org/browsh/releases/download/v1.4.12/browsh_1.4.12_linux_amd64.deb
  sudo dpkg -i browsh_1.4.12_linux_amd64.deb 
```
2-32-bit:
```bash
  wget https://github.com/browsh-org/browsh/releases/download/v1.4.12/browsh_1.4.12_linux_386.deb
  sudo dpkg -i browsh_1.4.12_linux_386.deb 
```

- from Source

1- 64-bit:
```bash
  wget https://github.com/browsh-org/browsh/releases/download/v1.4.12/browsh_1.4.12_linux_amd64
  chmod 755 browsh_1.4.12_linux_amd64
  ./browsh_1.4.12_linux_amd64
```
2- 64-bit:
```bash
  wget https://github.com/browsh-org/browsh/releases/download/v1.4.12/browsh_1.4.12_linux_386
  chmod 755 browsh_1.4.12_linux_386
  ./browsh_1.4.12_linux_386
```

[top](#top)

