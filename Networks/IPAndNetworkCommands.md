<div dir=rtl>بنام خدا</div>

- [Dealing with IP](#dealing-with-ip)
  - [Disabling with IPV6](#disabling-with-ipv6)
- [Package Management](#package-management)
  - [Yum](#yum)
- [Some NetWork Commands](#some-netWork-commands)
  - [ncat](#ncat)
  - [tcpflow](#tcpflow)


[top](#top)
# Dealing with IP
- [Disabling with IPV6](#disabling-with-ipv6)
### Disabling with IPV6:
1. In Redhat:
   - first way: in _/etc/sysconfig/network_ file
   ```vim
     NETWORKING=yes
     NETWORKING_IPV6=no
   ```
   - Second way: in _/etc/sysconfig/network-scripts/ifcfg-eth0_:
   ```vim
     IPV6INIT=no
     IPV6_AUTOCONF=no
     IPV6_DEFROUTE=no
     IPV6_PEERDNS=no
     IPV6_PEERROUTES=no
     IPV6_FAILURE_FATAL=no
   ```
2. In Ubuntu: in _/etc/sysctl.conf_ file:
```vim
  net.ipv6.conf.all.disable_ipv6 = 1
  net.ipv6.conf.default.disable_ipv6 = 1
  net.ipv6.conf.lo.disable_ipv6 = 1
```

[top](#top)
# Package Management
## Yum
- installing: 
  1. by Package[s] Name[s] -> `yum install PackageName1 PackageName2 ...`
  2. by Group Package Name -> `yum groupinstall "GroupName"`
- updatig: `yum update ...`
- upgrading: `yum upgrade ...`
- removeing: `yum remove ...`
- list Packages: 
  1. for all ->`yum list installed` 
  2. for group `yum grouplist GroupName` 
  3. for single `yum list PackageName`
- search for Package Name: `yum search Name`
- serach which Package Provide specific Command/file/...: `yum provides Name`
- List repositories: `yum repolist all`
- enable and disable a repo: `yum install ... -enablerepo=RepoName`
- clean Cache: `yum clean cache`
- history: `yum history`

[top](#top)
# Some NetWork Commands
- [ncat](#ncat)
- [tcpflow](#tcpflow)
#### ncat
- to send data on machine port and listening.
  1. listening: `nc -l PortNo.`
  2. sending: `cat File | nc IP PortNo.`
  - scan ports: `nc -zv IP 1-56555`
  - listen to the port : `nc -lk IP port`
  - live Send to the port: `nc -n IP port`
  - Send file: `nc IP Port < File`
  - Save reciver in File: `nc -l Port > File`
  - make simple server: `while true; do nc -l 8888 < index.html; done` & simply browse with `http://localhost:8888`
  - zip and senf file : `dd if=/dev/sda | gzip -9 | nc -l 33`&untar in reciever: `nc localhost 3333 | tar -zf -`
  - security connect to port: 
```bash
  ssh -f -L 233:127.0.0.1:33 me@IP sleep 10;nc localhost 233 | pv -b > backup.iso
```
[top](#top)
###### Tcpdump
```vim
  tcpdump -D              --> List of available NICs
  tcpdump -i NICName      --> Monitor a NIC
  tcpdump -c 8 -i NICName --> Limit Number of packets
  tcpdump -A              --> Print packets is ASCII
  tcpdump -XX             --> Display in HEX too
  tcpdump -n              --> Capture IP
  tcpdump ProtocolName    --> Capture Only this Protocol
  tcpdump src/dst IP      --> Log Only Packets from this IP Source/Destination
  tcpdump port No.        --> Sniff used port
  
```
* Listning on busy port:
```vim
  tcpflow   -i Interface -C[Print On Console] -J[Show Coloring] port PortNo
```

[top](#top)
