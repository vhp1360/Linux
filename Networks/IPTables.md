<div dir="rtl">بنام خدا</div>

- [IPTables Structure](#iptables-structure)
- [Make a Secure IPTables](#make-a-secure-iptables)
- [Logs and Creating Seperate Log File](#logs-and-creating-seperate-log-file)

[top](#top)

# IPTables Structure
1. CentOS

2. Ubuntu

```vim
 *filter
 :INPUT DROP [0:0]
 :FORWARD DROP [0:0]
 :OUTPUT ACCEPT [0:0]
 
 :UDP - [0:0]
 :TCP - [0:0]
 :ICMP - [0:0]
 COMMIT

 *raw
 :PREROUTING ACCEPT [0:0]
 :OUTPUT ACCEPT [0:0]
 COMMIT

 *nat
 :PREROUTING ACCEPT [0:0]
 :INPUT ACCEPT [0:0]
 :OUTPUT ACCEPT [0:0]
 :POSTROUTING ACCEPT [0:0]
 COMMIT

 *security
 :INPUT ACCEPT [0:0]
 :FORWARD ACCEPT [0:0]
 :OUTPUT ACCEPT [0:0]
 COMMIT

 *mangle
 :PREROUTING ACCEPT [0:0]
 :INPUT ACCEPT [0:0]
 :FORWARD ACCEPT [0:0]
 :OUTPUT ACCEPT [0:0]
 :POSTROUTING ACCEPT [0:0]
 COMMIT
```

[Top](#top)

# Make a Secure IPTables
+ Privent Ports Scan

```vim
  iptables -A INPUT -p tcp -i eth0 -m state --state NEW -m recent --set
  iptables -A INPUT -p tcp -i eth0 -m state --state NEW -m recent --update --seconds 30 --hitcount 10 -j DROP
```
+ connlimit:
  - limit number of ssh connection:

  ```vim
    iptables -A INPUT -p tcp --syn --dport 22 -m connlimit --connlimit-above 3 -j REJECT
  ```
 
[Top](#top)

# Logs And Creating Seperate Log File
- Logs: Followed by a level number or name. Valid names are (case-insensitive) :__debug__, __info__, __notice__, __warning__, __err__,__crit__, __alert__ and __emerg__ corresponding to numbers 7 through 0
  - to change log file save name:
     - First Way:

         ```vim
           echo "kern.warning /var/log/iptables.log">>/etc/syslog.conf
           service rsyslog restart
         ```
     - Second Way:
         * in CentOS:

            ```vim
              cat << EOF > /etc/rsyslog.d/iptables.conf
              :msg, contains, "iptables" -/var/log/iptables.log
              &~
              __EOF__
              # Prepare file and manage Permission
              touch /var/log/iptables.log
              chown root:root /var/log/iptables.log
              chmod 600 /var/log/iptables.log
              # Finnally Config log rotate
              service rsyslog reload
              cat << EOF > /etc/logrotate.d/iptables
              /var/log/iptables.log
              {
               rotate 7
               daily
               missingok
               notifempty
               delaycompress
               compress
               postrotate
                 /sbin/service rsyslog reload 2>&1 || true
               endscript
              }
              EOF
            ```
         * Ubuntu:

      ```vim
        cat << EOF > /etc/rsyslog.d/10-iptables.conf
        :msg, contains, "iptables: " -/var/log/iptables.log
        & ~
        EOF
        service rsyslog restart
      ```
  - Generating Logs:
    - simple : `iptables -A INPUT -j LOG`
    - next example : `iptables -A INPUT -s a.a.a.a -j LOG --log-prefix 'Source a.a.a.a **' --log-level 6`
    - to prevent flooding log file, limit log line per 5 minutes for 7 bursts
 
    ```vim
      iptables -A INPUT -s a.a.a.a -m limit --limit 5/m --limit-burst 7 -j LOG --log-prefix \
      'Source a.a.a.a **' --log-level 6
    ```
    - Logs everything:
 
    ```vim
      iptables -I INPUT 1 -j LOG
      iptables -I FORWARD 1 -j LOG
      iptables -I OUTPUT 1 -j LOG
      iptables -t nat -I PREROUTING 1 -j LOG
      iptables -t nat -I POSTROUTING 1 -j LOG
      iptables -t nat -I OUTPUT 1 -j LOG
    ```
    


[top](#top)
