<div dir="rtl" align="right">بنام خدا</div>

- [NIC Bonding](#nic-bonding)
  - [Definition](#definition)
  - [Bonding modes](#bonding-modes)
  - [Implimenting](#implimenting)
  - [Test](#test)
  - [Link Monitoring](#link-monitoring)


[top](#)
# NIC Bonding
## Definition
thanks to [THE GEEK DIARY site](https://www.thegeekdiary.com/centos-rhel-7-how-to-configure-network-bonding-or-nic-teaming/)

Network interface bonding is called by many names: Port Trunking, Channel Bonding, Link Aggregation, NIC teaming, and others. 
It combines or aggregates multiple network connections into a single channel bonding interface. 
This allows two or more network interfaces to act as one, to increase throughput and to provide redundancy or failover.
The Linux kernel comes with the bonding driver for aggregating multiple physical network interfaces into a single logical interface.
for example, aggregating eth0 and eth1 into bond0. For each bonded interface you can define the mode and the link monitoring options. 
There are seven different mode options, each providing specific load balancing and fault tolerance characteristics as shown in the table below.

[top](#)
### Bonding modes

Mode|Policy|How does it work|Fault Tolerance|Load balancing
---|---|---|---|---
0|Round Robin|packets are sequentially transmitted/received through each interfaces one by one.|:red_circle:|:white_check_mark:
1|Active Backup|one NIC active while a:red_circle:ther NIC is asleep. If the active NIC goes down, a:red_circle:ther NIC becomes active. only supported in x86 environments.|:white_check_mark:|:red_circle:
2|XOR [exclusive OR]|In this mode the, the MAC address of the slave NIC is matched up against the incoming request’s MAC and once this connection is established same NIC is used to transmit/receive for the destination MAC.|:white_check_mark:|:white_check_mark:
3|Broadcast|All transmissions are sent on all slaves|:white_check_mark:|:red_circle:
4|Dynamic Link Aggregation|aggregated NICs act as one NIC which results in a higher throughput, but also provides failover in the case that a NIC fails. Dynamic Link Aggregation requires a switch that supports IEEE 802.3ad.|:white_check_mark:|:white_check_mark:
5|Transmit Load Balancing (TLB)|The outgoing traffic is distributed depending on the current load on each slave interface. Incoming traffic is received by the current slave. If the receiving slave fails, a:red_circle:ther slave takes over the MAC address of the failed slave.|:white_check_mark:|:white_check_mark:
6|Adaptive Load Balancing (ALB)|Unlike Dynamic Link Aggregation, Adaptive Load Balancing does :red_circle:t require any particular switch configuration. Adaptive Load Balancing is only supported in x86 environments. The receiving packets are load balanced through ARP negotiation.|:white_check_mark:|:white_check_mark:

[top](#)
### Implimenting
Creating a Bonding Interface File
You can manually create a bonding interface file in the /etc/sysconfig/network-scripts directory. You first create the bonding interface and then you add the physical network interfaces to the bond. These physical network interfaces are called “slaves“.

For the example in this post, the slaves for the bond0 interface are ens33 and ens37. Before starting up, make sure the bonding module is loaded properly. To verify that, use the command shown below :
```bash
  lsmod |grep bonding
```
* If the module is not loaded, load it using modprobe command.
```bash
  modprobe bonding
```
1. create **Bound** Interface like _/etc/sysconfig/network-scripts/ifcfg-bond0_:
```vim
  DEVICE=bond0
  BONDING_OPTS="miimon=1 updelay=0 downdelay=0 mode=active-backup" TYPE=Bond
  BONDING_MASTER=yes
  BOOTPROTO=none
  IPADDR=192.168.2.12
  PREFIX=24
  DEFROUTE=yes
  IPV4_FAILURE_FATAL=no
  IPV6INIT=yes
  IPV6_AUTOCONF=yes
  IPV6_DEFROUTE=yes
  IPV6_FAILURE_FATAL=no
  IPV6_ADDR_GEN_MODE=stable-privacy
  NAME=bond0
  UUID=bbe539aa-5042-4d28-a0e6-2a4d4f5dd744
  ONBOOT=yes
```
2. config All Interfaces for example ens33 NIC as a slave for bond0 in _/etc/sysconfig/network-scripts/ifcfg-ens33 _:
```bash
  TYPE=Ethernet
  NAME=ens33
  UUID=817e285b-60f0-42d8-b259-4b62e21d823d
  DEVICE=ens33
  ONBOOT=yes
  MASTER=bond0
  SLAVE=yes
```
3. Restart the network services and up Bound Interface:
```bash
  systemctl restart network
  ifup bond0
```
[top](#)
### Test
- does link up:
```bash
  ip addr show
```
- Also verify current status of bonding interfaces and which interface is currently active, using the command below:
```bash
  cat /proc/net/bonding/bond0
```
output like:
> Ethernet Channel Bonding Driver: v3.7.1 (April 27, 2011)
> 
> Bonding Mode: fault-tolerance (active-backup)
Primary Slave: None
Currently Active Slave: ens33
MII Status: up
MII Polling Interval (ms): 1
Up Delay (ms): 0
Down Delay (ms): 0
> 
Slave Interface: ens33
MII Status: up
Speed: 1000 Mbps
Duplex: full
Link Failure Count: 0
Permanent HW addr: 00:0c:29:54:f7:20
Slave queue ID: 0
>
Slave Interface: ens37
MII Status: up
Speed: 1000 Mbps
Duplex: full
Link Failure Count: 0
Permanent HW addr: 00:0c:29:54:f7:34
Slave queue ID: 0
From the command output above, we can see that ens33 is the currently active slave in the bond.

- Testing fault tolerance of the bonding configuration
```bash
  ifdown ens33
  cat /proc/net/bonding/bond0
```

[top](#)
### Link Monitoring
The bonding driver supports two methods to monitor a slave’s link state:

##### MII (Media Independent Interface) monitor
This is the default, and recommended, link monitoring option.
It monitors the carrier state of the local network interface.
You can specify the monitoring frequency and the delay.
Delay times allow you to account for switch initialization.
##### ARP monitor
This sends ARP queries to peer systems on the network and uses the response as an indication that the link is up.
You can specify the monitoring frequency and target addresses.


[top](#)

