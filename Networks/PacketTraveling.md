<div dir="rtl">بنام خدا</div>

- [Packet Traveling](#packet-traveling)
  
[top](#)  
#### Packet Traveling
[return to this article](https://www.digitalocean.com/community/tutorials/a-deep-dive-into-iptables-and-netfilter-architecture)

1. Tables:
 - *PREROUTING*:left_right_arrow:**NF_IP_PRE_ROUTING** hook
   
   will be triggered by any incoming traffic very soon after entering the network stack. This hook is processed before any routing decisions have been made
 - *INPUT*:left_right_arrow:**NF_IP_LOCAL_IN** hook
   
   is triggered after an incoming packet has been routed if the packet is destined for the local system.
 - FORWARD:left_right_arrow:**NF_IP_FORWARD** hook
    
    is triggered after an incoming packet has been routed if the packet is to be forwarded to another host.
 - OUTPUT:left_right_arrow:**NF_IP_LOCAL_OUT** hook
    
    is triggered by any locally created outbound traffic as soon it hits the network stack.
 - POSTROUTING:left_right_arrow:**NF_IP_POST_ROUTING** hook
    
    is triggered by any outgoing or forwarded traffic after routing has taken place and just before being put out on the wire.

[top](#)  

Tables:arrow_down:/Chains:arrow_right:|**PREROUTING**|**INPUT**|**FORWARD**|**OUTPUT**|**POSTROUTING**
:---:|:---:|:---:|:---:|:---:|:---:|
(routing decision)||||:white_check_mark:||
raw|:white_check_mark:|||:white_check_mark:||
(Conntrack enabled)|:white_check_mark:|||:white_check_mark:||
mangle|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
nat(DNAT)|:white_check_mark:|||:white_check_mark:||
(routing decision)|:white_check_mark:|||:white_check_mark:||
filter||:white_check_mark:|:white_check_mark:|:white_check_mark:||
security||:white_check_mark:|:white_check_mark:|:white_check_mark:||
nat(SNAT)||:white_check_mark:|||:white_check_mark:|

[top](#)  
  - Incoming packets destined for the local system: **PREROUTING** :arrow_right: **INPUT**
  - Incoming packets destined to another host: **PREROUTING** :arrow_right: **FORWARD** :arrow_right: **POSTROUTING**
  - Locally generated packets: **OUTPUT** :arrow_right: **POSTROUTING**

   so, an _incoming_ packet destined for the _local_ system will first be evaluated against the **PREROUTING** chains of the **_raw, mangle_**, and **_nat_** tables. It will then traverse the **INPUT** chains of the **_mangle, filter, security_**, and **_nat_** tables before finally being delivered to the local socket.  
- IPTables Rules: Contain Two Parts
  * Matching: Clear
  * Targets: Tow disicion
      1- Terminating:
      2- Marking for Next:
[top](#)  
- _**contrack**_ : provides stateful option to track a packet, but what are states?
  * Available States:
      - NEW: When a packet arrives that is not associated with an existing connection, but is not invalid as a first
      - ESTABLISHED: A connection is changed from NEW to ESTABLISHED when it receives a valid response  
      For **TCP**:arrow_right:means a _SYN/ACK_,for **UDP & ICMP**:arrow_right:means a response where source and destination of the original packet are switched.
      - RELATED: Packets that are not part of an existing connection, but are associated with a connection already in the system.This could mean a helper connection, as is the case with FTP data transmission connections, or it could be ICMP responses to connection attempts by other protocols.
      - INVALID: Packets can be marked INVALID if they are not associated with an existing connection and aren't appropriate for opening a new connection, if they cannot be identified, or if they aren't routable among other reasons.
      - UNTRACKED: Packets can be marked as UNTRACKED if they've been targeted in a raw table chain to bypass tracking.
      - SNAT: A virtual state set when the source address has been altered by NAT operations. This is used by the connection tracking system so that it knows to change the source addresses back in reply packets.
      - DNAT: A virtual state set when the destination address has been altered by NAT operations. This is used by the connection tracking system so that it knows to change the destination address back when routing reply packets.


[top](#)
