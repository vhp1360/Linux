<div dir="rtl" align="right">بنام خدا</div>

- [Systemd](#systemd)
  - [Cheat Sheet](#cheat-sheet)
- [Sysctl](#sysctl)
  - [Items Config](#items-config)



[top](#)
# Systemd
- [Cheat Sheet](#cheat-sheet)
  - [View System Info Commands](#view-system-info-commands)
  - [System Commands](#system-commands)
  - [Service Commands](#service-commands)
  - [Log Commands](#log-commands)
 
[top](#)
### Cheat Sheet
#### View System Info Commands
**Command**	| **Description**
---|---
systemctl list-dependencies | show a unit’s dependecies
systemctl list-sockets | list the sockets
systemctl list-jobs | View active systemd jobs
systemctl list-unit-files | See unit files and their states
systemctl list-units | Show if units are loaded/active
systemctl get-default | List default target (like run level)

#### System Commands
**Command**	| **Description**
---|---
systemctl reboot | Reboot the system (reboot.target)
systemctl poweroff | Power off the system (poweroff.target)
systemctl emergency | Put in emergency mode (emergency.target)
systemctl default | Back to default target (multi-user.target)

[top](#)
#### Service Commands
**Command**	| **Description**
---|---
systemctl stop service | Stop a running service
systemctl start service | Start a service
systemctl restart service | Restart a running service
systemctl reload service | Reload all config files in service
systemctl status service | See if service is running/enabled
systemctl enable service | Enable a service to start on boot
systemctl disable service | Disable service–won’t start at boot
systemctl show service | Show properties of a service (or other units)
systemctl -H  host status network | Run any systemctl command remotely

[top](#)
#### Log Commands
**Command**	| **Description**
---|---
journalctl | Show all collected log messages
journalctl -u network.service | See network service messages
journalctl -f | Follow messages as they appear
journalctl -k | Show only kernel messages

[top](#)



[top](#)
# Sysctl
```bash
  net.ipv4.icmp_echo_ignore_all = 1 <------------------ Disable ICMP
  net.ipv4.icmp_echo_ignore_broadcasts = 1 <----------- Disable Broadcat
  kernel.dmesg_restrict = 1 <-------------------------- Disable unprivileged User to run demsg
  
```


[top](#)