<div dir="rtl" align="right">بتام خدا</div>
Sometimes an Admin needs to run and excute some codes, then should have information about most useful of them.
- [Ruby](#ruby)
- [Python](#python)
- [NodeJS](#nodejs)
- [Java](#java)

# Ruby

[top](#top)

# Pythom

[top](#top)

# NodeJS

[top](#top)

# Java
- Create Jar file:

  1- first create java file:
```java
  public class App {
    public static void main(String[] args){
       System.out.println(" Hello, I'm there! ");
    }
}
  ```
  2- generate class:
```java
  java -d file.java
```
  3- finally generate jar file:
```java
  java -cvf file.jar file.class
```

* it may when you try to run it with `java -jar file.jar` command you got **no main manifest attribute, in file.jar**, to fixed it,we create _vim MANIFEST.MF_ and ass it to existence jar file:

1- 
```java
  Main-Class:  App
```
2-
```java
  jar cvmf MANIFEST.MF file.jar file.class
```
  
[top](#top)